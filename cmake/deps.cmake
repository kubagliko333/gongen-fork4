include("${CMAKE_CURRENT_LIST_DIR}/versions.cmake")

# Downloading the CMake Package Manager
if(CPM_SOURCE_CACHE)
	set(CPM_DOWNLOAD_LOCATION "${CPM_SOURCE_CACHE}/cpm/CPM_${CPM_DOWNLOAD_VERSION}.cmake")
elseif(DEFINED ENV{CPM_SOURCE_CACHE})
	set(CPM_DOWNLOAD_LOCATION "$ENV{CPM_SOURCE_CACHE}/cpm/CPM_${CPM_DOWNLOAD_VERSION}.cmake")
else()
	set(CPM_DOWNLOAD_LOCATION "${CMAKE_BINARY_DIR}/cmake/CPM_${CPM_DOWNLOAD_VERSION}.cmake")
endif()

if(NOT (EXISTS ${CPM_DOWNLOAD_LOCATION}))
	message(STATUS "Downloading CPM.cmake to ${CPM_DOWNLOAD_LOCATION}")
	file(DOWNLOAD https://github.com/TheLartians/CPM.cmake/releases/download/v${CPM_DOWNLOAD_VERSION}/CPM.cmake ${CPM_DOWNLOAD_LOCATION})
endif()

include(${CPM_DOWNLOAD_LOCATION})

# Adding dependencies
macro(gen_add_dependency name)
	cmake_language(CALL gen_add_dependency_internal_${name})
endmacro()

macro(gen_add_package_internal_macro name repo)
	CPMAddPackage(
		NAME ${name}
		GITHUB_REPOSITORY ${repo}
		GIT_TAG ${${name}_TAG}
		${ARGN}
	)

	FetchContent_GetProperties(${name})
endmacro()

macro(gen_add_dependency_internal_glfw)
	gen_add_package_internal_macro(glfw "glfw/glfw"
		OPTIONS
			"BUILD_SHARED_LIBS OFF"
			"GLFW_INSTALL OFF"
			"GLFW_BUILD_DOCS OFF"
			"GLFW_BUILD_TESTS OFF"
			"GLFW_BUILD_EXAMPLES OFF"
	)
endmacro()

macro(gen_add_dependency_internal_tinyxml2)
	gen_add_package_internal_macro(tinyxml2 "leethomason/tinyxml2"
		OPTIONS
			"BUILD_SHARED_LIBS OFF"
			"tinyxml2_BUILD_TESTING OFF"
	)
endmacro()

macro(gen_add_dependency_internal_stb)
	gen_add_package_internal_macro(stb "nothings/stb"
		DOWNLOAD_ONLY TRUE
	)
endmacro()

macro(gen_add_dependency_internal_zstd)
	gen_add_package_internal_macro(zstd "facebook/zstd"
		DOWNLOAD_ONLY TRUE
	)
	if (zstd_ADDED)
		set(ZSTD_LEGACY_SUPPORT OFF CACHE BOOL "")
		set(ZSTD_MULTITHREAD_SUPPORT OFF CACHE BOOL "")
		set(ZSTD_BUILD_PROGRAMS OFF CACHE BOOL "")
		set(ZSTD_BUILD_TESTS OFF CACHE BOOL "")
		set(ZSTD_BUILD_CONTRIB OFF CACHE BOOL "")
		set(ZSTD_BUILD_SHARED OFF CACHE BOOL "")
		set(ZSTD_BUILD_STATIC ON CACHE BOOL "")

		add_subdirectory("${zstd_SOURCE_DIR}/build/cmake" ${zstd_BINARY_DIR})
	endif()
endmacro()

macro(gen_add_dependency_internal_vulkan_headers)
	gen_add_package_internal_macro(vulkan_headers "KhronosGroup/Vulkan-Headers"
		DOWNLOAD_ONLY TRUE
	)
endmacro()

macro(gen_add_dependency_internal_volk)
	gen_add_package_internal_macro(volk "zeux/volk"
		DOWNLOAD_ONLY TRUE
	)
endmacro()

macro(gen_add_dependency_internal_vkmemalloc)
	gen_add_package_internal_macro(vkmemalloc "GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator"
		DOWNLOAD_ONLY TRUE
	)
endmacro()

macro(gen_add_dependency_internal_imgui)
	gen_add_package_internal_macro(imgui "ocornut/imgui"
		DOWNLOAD_ONLY TRUE
	)
endmacro()

macro(gen_add_dependency_internal_cpmlicenses)
	gen_add_package_internal_macro(cpmlicenses "cpm-cmake/CPMLicenses.cmake")
endmacro()

macro(gen_add_dependency_internal_bc7enc)
	gen_add_package_internal_macro(bc7enc "richgel999/bc7enc"
		DOWNLOAD_ONLY TRUE
	)

	if (bc7enc_ADDED)
		add_library(bc7enc "${bc7enc_SOURCE_DIR}/bc7enc.c")

		target_include_directories(bc7enc PUBLIC "${bc7enc_SOURCE_DIR}")
	endif()
endmacro()

macro(gen_add_dependency_internal_freetype)
	gen_add_package_internal_macro(freetype "freetype/freetype"
		OPTIONS "FT_DISABLE_ZLIB ON" "FT_DISABLE_BZIP2 ON" 
		"FT_DISABLE_PNG ON" "FT_DISABLE_HARFBUZZ ON" "FT_DISABLE_BROTLI ON"
	)
endmacro()

macro(gen_add_dependency_internal_msdfgen)
	gen_add_package_internal_macro(msdfgen "Chlumsky/msdfgen"
		DOWNLOAD_ONLY TRUE
	)

	if (msdfgen_ADDED)
		file(GLOB_RECURSE msdfgen_src "${msdfgen_SOURCE_DIR}/core/*.cpp")
		file(GLOB_RECURSE msdfgen_headers  "${msdfgen_SOURCE_DIR}/core/*.h" "${msdfgen_SOURCE_DIR}/core/*.hpp")

		add_library(msdfgen STATIC ${msdfgen_src})
		target_include_directories(msdfgen INTERFACE ${msdfgen_headers})
	endif()
endmacro()
