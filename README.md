# GongEn
A C++20 game engine by Bitrium licensed under the **[BSD 3-Clause "New" or "Revised" License](https://spdx.org/licenses/BSD-3-Clause.html)**.  
GongEn is still under heavy developement in a pre-alpha phase and therefore the engine shall not be used for serioues projects yet.
## Links
- [Trello board](https://trello.com/b/A12FaKqf/gongen) - project management, roadmap.
- [Discord](https://discord.gg/VuFjWc4Aar) - discussion, support, announcements.
## Usage
### Compiling
For GongEn compilation, use [CMake](https://cmake.org/).
## Versioning
GongEn uses a custom 4-part versioning system defined as follows: `MSTON.MAJOR.MINOR.PATCH`, where
| Part | Full name | Description |
| - | - | - |
| `MSTON` | Milestone | Represents a milestone in the GongEn's version history. New milestone versions may remove deprecated APIs and overall break backwards compatibility. A good moment to modernize the engine's code. |
| `MAJOR` | Major | Incremented when incompatible API changes are made. |
| `MINOR` | Minor | Features are added without breaking backwards compatibility. |
| `PATCH` | Patch | Bug fixes and small implementation changes. |

The last 3 parts are similar to the [Semantic Versioning standard](https://semver.org/).  
GongEn with the milestone version `X` will be referred to as "GongEn X". "GongEn Alpha" refers to versions before the first milestone. During the alpha phase the engine will be generally unstable and backwards incompatible changes will occur very often and might happen in minor releases as well.  
The first alpha release will be named `0.0.0.1`.
## Documentation
### Doxygen
We use [Doxygen](https://www.doxygen.nl/) for generating GongEn's code documentation.  
Doxygen's configuration file is `Doxyfile` (the default name). You can use `doxygen -u` to get docs for each value in the config file, but remember to change it back to stripped docs with `doxygen -s -u` before commiting.

To generate the documentation, simply use the `doxygen` command.