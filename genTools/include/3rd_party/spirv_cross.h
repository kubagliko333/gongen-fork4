/*
 * Copyright 2019-2021 Hans-Kristian Arntzen
 * SPDX-License-Identifier: Apache-2.0 OR MIT
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /*
  * At your option, you may choose to accept this material under either:
  *  1. The Apache License, Version 2.0, found at <http://www.apache.org/licenses/LICENSE-2.0>, or
  *  2. The MIT License, found at <http://opensource.org/licenses/MIT>.
  */

#ifndef SPIRV_CROSS_C_API_H
#define SPIRV_CROSS_C_API_H

#include <stddef.h>
  /*
   * C89-compatible wrapper for SPIRV-Cross' API.
   * Documentation here is sparse unless the behavior does not map 1:1 with C++ API.
   * It is recommended to look at the canonical C++ API for more detailed information.
   */

	/* Bumped if ABI or API breaks backwards compatibility. */
#define SPVC_C_API_VERSION_MAJOR 0
/* Bumped if APIs or enumerations are added in a backwards compatible way. */
#define SPVC_C_API_VERSION_MINOR 49
/* Bumped if internal implementation details change. */
#define SPVC_C_API_VERSION_PATCH 0

#if !defined(SPVC_PUBLIC_API)
#if defined(SPVC_EXPORT_SYMBOLS)
/* Exports symbols. Standard C calling convention is used. */
#if defined(__GNUC__)
#define SPVC_PUBLIC_API __attribute__((visibility("default")))
#elif defined(_MSC_VER)
#define SPVC_PUBLIC_API __declspec(dllexport)
#else
#define SPVC_PUBLIC_API
#endif
#else
#define SPVC_PUBLIC_API
#endif
#endif

typedef struct spvc_context_s* spvc_context;
typedef struct spvc_parsed_ir_s* spvc_parsed_ir;
typedef struct spvc_compiler_s* spvc_compiler;
typedef struct spvc_compiler_options_s* spvc_compiler_options;
typedef struct spvc_resources_s* spvc_resources;
struct spvc_type_s;
typedef const struct spvc_type_s* spvc_type;
typedef struct spvc_constant_s* spvc_constant;
struct spvc_set_s;
typedef const struct spvc_set_s* spvc_set;

typedef unsigned int SpvId;

typedef enum SpvExecutionModel_ {
	SpvExecutionModelVertex = 0,
	SpvExecutionModelTessellationControl = 1,
	SpvExecutionModelTessellationEvaluation = 2,
	SpvExecutionModelGeometry = 3,
	SpvExecutionModelFragment = 4,
	SpvExecutionModelGLCompute = 5,
	SpvExecutionModelKernel = 6,
	SpvExecutionModelTaskNV = 5267,
	SpvExecutionModelMeshNV = 5268,
	SpvExecutionModelRayGenerationKHR = 5313,
	SpvExecutionModelRayGenerationNV = 5313,
	SpvExecutionModelIntersectionKHR = 5314,
	SpvExecutionModelIntersectionNV = 5314,
	SpvExecutionModelAnyHitKHR = 5315,
	SpvExecutionModelAnyHitNV = 5315,
	SpvExecutionModelClosestHitKHR = 5316,
	SpvExecutionModelClosestHitNV = 5316,
	SpvExecutionModelMissKHR = 5317,
	SpvExecutionModelMissNV = 5317,
	SpvExecutionModelCallableKHR = 5318,
	SpvExecutionModelCallableNV = 5318,
	SpvExecutionModelMax = 0x7fffffff,
} SpvExecutionModel;

typedef enum SpvBuiltIn_ {
    SpvBuiltInPosition = 0,
    SpvBuiltInPointSize = 1,
    SpvBuiltInClipDistance = 3,
    SpvBuiltInCullDistance = 4,
    SpvBuiltInVertexId = 5,
    SpvBuiltInInstanceId = 6,
    SpvBuiltInPrimitiveId = 7,
    SpvBuiltInInvocationId = 8,
    SpvBuiltInLayer = 9,
    SpvBuiltInViewportIndex = 10,
    SpvBuiltInTessLevelOuter = 11,
    SpvBuiltInTessLevelInner = 12,
    SpvBuiltInTessCoord = 13,
    SpvBuiltInPatchVertices = 14,
    SpvBuiltInFragCoord = 15,
    SpvBuiltInPointCoord = 16,
    SpvBuiltInFrontFacing = 17,
    SpvBuiltInSampleId = 18,
    SpvBuiltInSamplePosition = 19,
    SpvBuiltInSampleMask = 20,
    SpvBuiltInFragDepth = 22,
    SpvBuiltInHelperInvocation = 23,
    SpvBuiltInNumWorkgroups = 24,
    SpvBuiltInWorkgroupSize = 25,
    SpvBuiltInWorkgroupId = 26,
    SpvBuiltInLocalInvocationId = 27,
    SpvBuiltInGlobalInvocationId = 28,
    SpvBuiltInLocalInvocationIndex = 29,
    SpvBuiltInWorkDim = 30,
    SpvBuiltInGlobalSize = 31,
    SpvBuiltInEnqueuedWorkgroupSize = 32,
    SpvBuiltInGlobalOffset = 33,
    SpvBuiltInGlobalLinearId = 34,
    SpvBuiltInSubgroupSize = 36,
    SpvBuiltInSubgroupMaxSize = 37,
    SpvBuiltInNumSubgroups = 38,
    SpvBuiltInNumEnqueuedSubgroups = 39,
    SpvBuiltInSubgroupId = 40,
    SpvBuiltInSubgroupLocalInvocationId = 41,
    SpvBuiltInVertexIndex = 42,
    SpvBuiltInInstanceIndex = 43,
    SpvBuiltInSubgroupEqMask = 4416,
    SpvBuiltInSubgroupEqMaskKHR = 4416,
    SpvBuiltInSubgroupGeMask = 4417,
    SpvBuiltInSubgroupGeMaskKHR = 4417,
    SpvBuiltInSubgroupGtMask = 4418,
    SpvBuiltInSubgroupGtMaskKHR = 4418,
    SpvBuiltInSubgroupLeMask = 4419,
    SpvBuiltInSubgroupLeMaskKHR = 4419,
    SpvBuiltInSubgroupLtMask = 4420,
    SpvBuiltInSubgroupLtMaskKHR = 4420,
    SpvBuiltInBaseVertex = 4424,
    SpvBuiltInBaseInstance = 4425,
    SpvBuiltInDrawIndex = 4426,
    SpvBuiltInPrimitiveShadingRateKHR = 4432,
    SpvBuiltInDeviceIndex = 4438,
    SpvBuiltInViewIndex = 4440,
    SpvBuiltInShadingRateKHR = 4444,
    SpvBuiltInBaryCoordNoPerspAMD = 4992,
    SpvBuiltInBaryCoordNoPerspCentroidAMD = 4993,
    SpvBuiltInBaryCoordNoPerspSampleAMD = 4994,
    SpvBuiltInBaryCoordSmoothAMD = 4995,
    SpvBuiltInBaryCoordSmoothCentroidAMD = 4996,
    SpvBuiltInBaryCoordSmoothSampleAMD = 4997,
    SpvBuiltInBaryCoordPullModelAMD = 4998,
    SpvBuiltInFragStencilRefEXT = 5014,
    SpvBuiltInViewportMaskNV = 5253,
    SpvBuiltInSecondaryPositionNV = 5257,
    SpvBuiltInSecondaryViewportMaskNV = 5258,
    SpvBuiltInPositionPerViewNV = 5261,
    SpvBuiltInViewportMaskPerViewNV = 5262,
    SpvBuiltInFullyCoveredEXT = 5264,
    SpvBuiltInTaskCountNV = 5274,
    SpvBuiltInPrimitiveCountNV = 5275,
    SpvBuiltInPrimitiveIndicesNV = 5276,
    SpvBuiltInClipDistancePerViewNV = 5277,
    SpvBuiltInCullDistancePerViewNV = 5278,
    SpvBuiltInLayerPerViewNV = 5279,
    SpvBuiltInMeshViewCountNV = 5280,
    SpvBuiltInMeshViewIndicesNV = 5281,
    SpvBuiltInBaryCoordKHR = 5286,
    SpvBuiltInBaryCoordNV = 5286,
    SpvBuiltInBaryCoordNoPerspKHR = 5287,
    SpvBuiltInBaryCoordNoPerspNV = 5287,
    SpvBuiltInFragSizeEXT = 5292,
    SpvBuiltInFragmentSizeNV = 5292,
    SpvBuiltInFragInvocationCountEXT = 5293,
    SpvBuiltInInvocationsPerPixelNV = 5293,
    SpvBuiltInLaunchIdKHR = 5319,
    SpvBuiltInLaunchIdNV = 5319,
    SpvBuiltInLaunchSizeKHR = 5320,
    SpvBuiltInLaunchSizeNV = 5320,
    SpvBuiltInWorldRayOriginKHR = 5321,
    SpvBuiltInWorldRayOriginNV = 5321,
    SpvBuiltInWorldRayDirectionKHR = 5322,
    SpvBuiltInWorldRayDirectionNV = 5322,
    SpvBuiltInObjectRayOriginKHR = 5323,
    SpvBuiltInObjectRayOriginNV = 5323,
    SpvBuiltInObjectRayDirectionKHR = 5324,
    SpvBuiltInObjectRayDirectionNV = 5324,
    SpvBuiltInRayTminKHR = 5325,
    SpvBuiltInRayTminNV = 5325,
    SpvBuiltInRayTmaxKHR = 5326,
    SpvBuiltInRayTmaxNV = 5326,
    SpvBuiltInInstanceCustomIndexKHR = 5327,
    SpvBuiltInInstanceCustomIndexNV = 5327,
    SpvBuiltInObjectToWorldKHR = 5330,
    SpvBuiltInObjectToWorldNV = 5330,
    SpvBuiltInWorldToObjectKHR = 5331,
    SpvBuiltInWorldToObjectNV = 5331,
    SpvBuiltInHitTNV = 5332,
    SpvBuiltInHitKindKHR = 5333,
    SpvBuiltInHitKindNV = 5333,
    SpvBuiltInCurrentRayTimeNV = 5334,
    SpvBuiltInIncomingRayFlagsKHR = 5351,
    SpvBuiltInIncomingRayFlagsNV = 5351,
    SpvBuiltInRayGeometryIndexKHR = 5352,
    SpvBuiltInWarpsPerSMNV = 5374,
    SpvBuiltInSMCountNV = 5375,
    SpvBuiltInWarpIDNV = 5376,
    SpvBuiltInSMIDNV = 5377,
    SpvBuiltInMax = 0x7fffffff,
} SpvBuiltIn;

/*
 * Shallow typedefs. All SPIR-V IDs are plain 32-bit numbers, but this helps communicate which data is used.
 * Maps to a SPIRType.
 */
typedef SpvId spvc_type_id;
/* Maps to a SPIRVariable. */
typedef SpvId spvc_variable_id;
/* Maps to a SPIRConstant. */
typedef SpvId spvc_constant_id;

/* See C++ API. */
typedef struct spvc_reflected_resource
{
	spvc_variable_id id;
	spvc_type_id base_type_id;
	spvc_type_id type_id;
	const char* name;
} spvc_reflected_resource;

typedef struct spvc_reflected_builtin_resource
{
	SpvBuiltIn builtin;
	spvc_type_id value_type_id;
	spvc_reflected_resource resource;
} spvc_reflected_builtin_resource;

/* See C++ API. */
typedef struct spvc_entry_point
{
	SpvExecutionModel execution_model;
	const char* name;
} spvc_entry_point;

/* See C++ API. */
typedef struct spvc_combined_image_sampler
{
	spvc_variable_id combined_id;
	spvc_variable_id image_id;
	spvc_variable_id sampler_id;
} spvc_combined_image_sampler;

/* See C++ API. */
typedef struct spvc_specialization_constant
{
	spvc_constant_id id;
	unsigned constant_id;
} spvc_specialization_constant;

/* See C++ API. */
typedef struct spvc_buffer_range
{
	unsigned index;
	size_t offset;
	size_t range;
} spvc_buffer_range;

/* See C++ API. */
typedef struct spvc_hlsl_root_constants
{
	unsigned start;
	unsigned end;
	unsigned binding;
	unsigned space;
} spvc_hlsl_root_constants;

/* See C++ API. */
typedef struct spvc_hlsl_vertex_attribute_remap
{
	unsigned location;
	const char* semantic;
} spvc_hlsl_vertex_attribute_remap;

/*
 * Be compatible with non-C99 compilers, which do not have stdbool.
 * Only recent MSVC compilers supports this for example, and ideally SPIRV-Cross should be linkable
 * from a wide range of compilers in its C wrapper.
 */
typedef unsigned char spvc_bool;
#define SPVC_TRUE ((spvc_bool)1)
#define SPVC_FALSE ((spvc_bool)0)

typedef enum spvc_result
{
	/* Success. */
	SPVC_SUCCESS = 0,

	/* The SPIR-V is invalid. Should have been caught by validation ideally. */
	SPVC_ERROR_INVALID_SPIRV = -1,

	/* The SPIR-V might be valid or invalid, but SPIRV-Cross currently cannot correctly translate this to your target language. */
	SPVC_ERROR_UNSUPPORTED_SPIRV = -2,

	/* If for some reason we hit this, new or malloc failed. */
	SPVC_ERROR_OUT_OF_MEMORY = -3,

	/* Invalid API argument. */
	SPVC_ERROR_INVALID_ARGUMENT = -4,

	SPVC_ERROR_INT_MAX = 0x7fffffff
} spvc_result;

typedef enum spvc_capture_mode
{
	/* The Parsed IR payload will be copied, and the handle can be reused to create other compiler instances. */
	SPVC_CAPTURE_MODE_COPY = 0,

	/*
	 * The payload will now be owned by the compiler.
	 * parsed_ir should now be considered a dead blob and must not be used further.
	 * This is optimal for performance and should be the go-to option.
	 */
	 SPVC_CAPTURE_MODE_TAKE_OWNERSHIP = 1,

	 SPVC_CAPTURE_MODE_INT_MAX = 0x7fffffff
} spvc_capture_mode;

typedef enum spvc_backend
{
	/* This backend can only perform reflection, no compiler options are supported. Maps to spirv_cross::Compiler. */
	SPVC_BACKEND_NONE = 0,
	SPVC_BACKEND_GLSL = 1, /* spirv_cross::CompilerGLSL */
	SPVC_BACKEND_HLSL = 2, /* CompilerHLSL */
	SPVC_BACKEND_MSL = 3, /* CompilerMSL */
	SPVC_BACKEND_CPP = 4, /* CompilerCPP */
	SPVC_BACKEND_JSON = 5, /* CompilerReflection w/ JSON backend */
	SPVC_BACKEND_INT_MAX = 0x7fffffff
} spvc_backend;

/* Maps to C++ API. */
typedef enum spvc_resource_type
{
	SPVC_RESOURCE_TYPE_UNKNOWN = 0,
	SPVC_RESOURCE_TYPE_UNIFORM_BUFFER = 1,
	SPVC_RESOURCE_TYPE_STORAGE_BUFFER = 2,
	SPVC_RESOURCE_TYPE_STAGE_INPUT = 3,
	SPVC_RESOURCE_TYPE_STAGE_OUTPUT = 4,
	SPVC_RESOURCE_TYPE_SUBPASS_INPUT = 5,
	SPVC_RESOURCE_TYPE_STORAGE_IMAGE = 6,
	SPVC_RESOURCE_TYPE_SAMPLED_IMAGE = 7,
	SPVC_RESOURCE_TYPE_ATOMIC_COUNTER = 8,
	SPVC_RESOURCE_TYPE_PUSH_CONSTANT = 9,
	SPVC_RESOURCE_TYPE_SEPARATE_IMAGE = 10,
	SPVC_RESOURCE_TYPE_SEPARATE_SAMPLERS = 11,
	SPVC_RESOURCE_TYPE_ACCELERATION_STRUCTURE = 12,
	SPVC_RESOURCE_TYPE_RAY_QUERY = 13,
	SPVC_RESOURCE_TYPE_INT_MAX = 0x7fffffff
} spvc_resource_type;

typedef enum spvc_builtin_resource_type
{
	SPVC_BUILTIN_RESOURCE_TYPE_UNKNOWN = 0,
	SPVC_BUILTIN_RESOURCE_TYPE_STAGE_INPUT = 1,
	SPVC_BUILTIN_RESOURCE_TYPE_STAGE_OUTPUT = 2,
	SPVC_BUILTIN_RESOURCE_TYPE_INT_MAX = 0x7fffffff
} spvc_builtin_resource_type;

/* Maps to spirv_cross::SPIRType::BaseType. */
typedef enum spvc_basetype
{
	SPVC_BASETYPE_UNKNOWN = 0,
	SPVC_BASETYPE_VOID = 1,
	SPVC_BASETYPE_BOOLEAN = 2,
	SPVC_BASETYPE_INT8 = 3,
	SPVC_BASETYPE_UINT8 = 4,
	SPVC_BASETYPE_INT16 = 5,
	SPVC_BASETYPE_UINT16 = 6,
	SPVC_BASETYPE_INT32 = 7,
	SPVC_BASETYPE_UINT32 = 8,
	SPVC_BASETYPE_INT64 = 9,
	SPVC_BASETYPE_UINT64 = 10,
	SPVC_BASETYPE_ATOMIC_COUNTER = 11,
	SPVC_BASETYPE_FP16 = 12,
	SPVC_BASETYPE_FP32 = 13,
	SPVC_BASETYPE_FP64 = 14,
	SPVC_BASETYPE_STRUCT = 15,
	SPVC_BASETYPE_IMAGE = 16,
	SPVC_BASETYPE_SAMPLED_IMAGE = 17,
	SPVC_BASETYPE_SAMPLER = 18,
	SPVC_BASETYPE_ACCELERATION_STRUCTURE = 19,

	SPVC_BASETYPE_INT_MAX = 0x7fffffff
} spvc_basetype;

#define SPVC_COMPILER_OPTION_COMMON_BIT 0x1000000
#define SPVC_COMPILER_OPTION_GLSL_BIT 0x2000000
#define SPVC_COMPILER_OPTION_HLSL_BIT 0x4000000
#define SPVC_COMPILER_OPTION_MSL_BIT 0x8000000
#define SPVC_COMPILER_OPTION_LANG_BITS 0x0f000000
#define SPVC_COMPILER_OPTION_ENUM_BITS 0xffffff


typedef enum spvc_compiler_option
{
	SPVC_COMPILER_OPTION_UNKNOWN = 0,

	SPVC_COMPILER_OPTION_FORCE_TEMPORARY = 1 | SPVC_COMPILER_OPTION_COMMON_BIT,
	SPVC_COMPILER_OPTION_FLATTEN_MULTIDIMENSIONAL_ARRAYS = 2 | SPVC_COMPILER_OPTION_COMMON_BIT,
	SPVC_COMPILER_OPTION_FIXUP_DEPTH_CONVENTION = 3 | SPVC_COMPILER_OPTION_COMMON_BIT,
	SPVC_COMPILER_OPTION_FLIP_VERTEX_Y = 4 | SPVC_COMPILER_OPTION_COMMON_BIT,

	SPVC_COMPILER_OPTION_GLSL_SUPPORT_NONZERO_BASE_INSTANCE = 5 | SPVC_COMPILER_OPTION_GLSL_BIT,
	SPVC_COMPILER_OPTION_GLSL_SEPARATE_SHADER_OBJECTS = 6 | SPVC_COMPILER_OPTION_GLSL_BIT,
	SPVC_COMPILER_OPTION_GLSL_ENABLE_420PACK_EXTENSION = 7 | SPVC_COMPILER_OPTION_GLSL_BIT,
	SPVC_COMPILER_OPTION_GLSL_VERSION = 8 | SPVC_COMPILER_OPTION_GLSL_BIT,
	SPVC_COMPILER_OPTION_GLSL_ES = 9 | SPVC_COMPILER_OPTION_GLSL_BIT,
	SPVC_COMPILER_OPTION_GLSL_VULKAN_SEMANTICS = 10 | SPVC_COMPILER_OPTION_GLSL_BIT,
	SPVC_COMPILER_OPTION_GLSL_ES_DEFAULT_FLOAT_PRECISION_HIGHP = 11 | SPVC_COMPILER_OPTION_GLSL_BIT,
	SPVC_COMPILER_OPTION_GLSL_ES_DEFAULT_INT_PRECISION_HIGHP = 12 | SPVC_COMPILER_OPTION_GLSL_BIT,

	SPVC_COMPILER_OPTION_HLSL_SHADER_MODEL = 13 | SPVC_COMPILER_OPTION_HLSL_BIT,
	SPVC_COMPILER_OPTION_HLSL_POINT_SIZE_COMPAT = 14 | SPVC_COMPILER_OPTION_HLSL_BIT,
	SPVC_COMPILER_OPTION_HLSL_POINT_COORD_COMPAT = 15 | SPVC_COMPILER_OPTION_HLSL_BIT,
	SPVC_COMPILER_OPTION_HLSL_SUPPORT_NONZERO_BASE_VERTEX_BASE_INSTANCE = 16 | SPVC_COMPILER_OPTION_HLSL_BIT,

	SPVC_COMPILER_OPTION_MSL_VERSION = 17 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_TEXEL_BUFFER_TEXTURE_WIDTH = 18 | SPVC_COMPILER_OPTION_MSL_BIT,

	/* Obsolete, use SWIZZLE_BUFFER_INDEX instead. */
	SPVC_COMPILER_OPTION_MSL_AUX_BUFFER_INDEX = 19 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_SWIZZLE_BUFFER_INDEX = 19 | SPVC_COMPILER_OPTION_MSL_BIT,

	SPVC_COMPILER_OPTION_MSL_INDIRECT_PARAMS_BUFFER_INDEX = 20 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_SHADER_OUTPUT_BUFFER_INDEX = 21 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_SHADER_PATCH_OUTPUT_BUFFER_INDEX = 22 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_SHADER_TESS_FACTOR_OUTPUT_BUFFER_INDEX = 23 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_SHADER_INPUT_WORKGROUP_INDEX = 24 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_ENABLE_POINT_SIZE_BUILTIN = 25 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_DISABLE_RASTERIZATION = 26 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_CAPTURE_OUTPUT_TO_BUFFER = 27 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_SWIZZLE_TEXTURE_SAMPLES = 28 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_PAD_FRAGMENT_OUTPUT_COMPONENTS = 29 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_TESS_DOMAIN_ORIGIN_LOWER_LEFT = 30 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_PLATFORM = 31 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_ARGUMENT_BUFFERS = 32 | SPVC_COMPILER_OPTION_MSL_BIT,

	SPVC_COMPILER_OPTION_GLSL_EMIT_PUSH_CONSTANT_AS_UNIFORM_BUFFER = 33 | SPVC_COMPILER_OPTION_GLSL_BIT,

	SPVC_COMPILER_OPTION_MSL_TEXTURE_BUFFER_NATIVE = 34 | SPVC_COMPILER_OPTION_MSL_BIT,

	SPVC_COMPILER_OPTION_GLSL_EMIT_UNIFORM_BUFFER_AS_PLAIN_UNIFORMS = 35 | SPVC_COMPILER_OPTION_GLSL_BIT,

	SPVC_COMPILER_OPTION_MSL_BUFFER_SIZE_BUFFER_INDEX = 36 | SPVC_COMPILER_OPTION_MSL_BIT,

	SPVC_COMPILER_OPTION_EMIT_LINE_DIRECTIVES = 37 | SPVC_COMPILER_OPTION_COMMON_BIT,

	SPVC_COMPILER_OPTION_MSL_MULTIVIEW = 38 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_VIEW_MASK_BUFFER_INDEX = 39 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_DEVICE_INDEX = 40 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_VIEW_INDEX_FROM_DEVICE_INDEX = 41 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_DISPATCH_BASE = 42 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_DYNAMIC_OFFSETS_BUFFER_INDEX = 43 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_TEXTURE_1D_AS_2D = 44 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_ENABLE_BASE_INDEX_ZERO = 45 | SPVC_COMPILER_OPTION_MSL_BIT,

	/* Obsolete. Use MSL_FRAMEBUFFER_FETCH_SUBPASS instead. */
	SPVC_COMPILER_OPTION_MSL_IOS_FRAMEBUFFER_FETCH_SUBPASS = 46 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_FRAMEBUFFER_FETCH_SUBPASS = 46 | SPVC_COMPILER_OPTION_MSL_BIT,

	SPVC_COMPILER_OPTION_MSL_INVARIANT_FP_MATH = 47 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_EMULATE_CUBEMAP_ARRAY = 48 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_ENABLE_DECORATION_BINDING = 49 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_FORCE_ACTIVE_ARGUMENT_BUFFER_RESOURCES = 50 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_FORCE_NATIVE_ARRAYS = 51 | SPVC_COMPILER_OPTION_MSL_BIT,

	SPVC_COMPILER_OPTION_ENABLE_STORAGE_IMAGE_QUALIFIER_DEDUCTION = 52 | SPVC_COMPILER_OPTION_COMMON_BIT,

	SPVC_COMPILER_OPTION_HLSL_FORCE_STORAGE_BUFFER_AS_UAV = 53 | SPVC_COMPILER_OPTION_HLSL_BIT,

	SPVC_COMPILER_OPTION_FORCE_ZERO_INITIALIZED_VARIABLES = 54 | SPVC_COMPILER_OPTION_COMMON_BIT,

	SPVC_COMPILER_OPTION_HLSL_NONWRITABLE_UAV_TEXTURE_AS_SRV = 55 | SPVC_COMPILER_OPTION_HLSL_BIT,

	SPVC_COMPILER_OPTION_MSL_ENABLE_FRAG_OUTPUT_MASK = 56 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_ENABLE_FRAG_DEPTH_BUILTIN = 57 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_ENABLE_FRAG_STENCIL_REF_BUILTIN = 58 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_ENABLE_CLIP_DISTANCE_USER_VARYING = 59 | SPVC_COMPILER_OPTION_MSL_BIT,

	SPVC_COMPILER_OPTION_HLSL_ENABLE_16BIT_TYPES = 60 | SPVC_COMPILER_OPTION_HLSL_BIT,

	SPVC_COMPILER_OPTION_MSL_MULTI_PATCH_WORKGROUP = 61 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_SHADER_INPUT_BUFFER_INDEX = 62 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_SHADER_INDEX_BUFFER_INDEX = 63 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_VERTEX_FOR_TESSELLATION = 64 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_VERTEX_INDEX_TYPE = 65 | SPVC_COMPILER_OPTION_MSL_BIT,

	SPVC_COMPILER_OPTION_GLSL_FORCE_FLATTENED_IO_BLOCKS = 66 | SPVC_COMPILER_OPTION_GLSL_BIT,

	SPVC_COMPILER_OPTION_MSL_MULTIVIEW_LAYERED_RENDERING = 67 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_ARRAYED_SUBPASS_INPUT = 68 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_R32UI_LINEAR_TEXTURE_ALIGNMENT = 69 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_R32UI_ALIGNMENT_CONSTANT_ID = 70 | SPVC_COMPILER_OPTION_MSL_BIT,

	SPVC_COMPILER_OPTION_HLSL_FLATTEN_MATRIX_VERTEX_INPUT_SEMANTICS = 71 | SPVC_COMPILER_OPTION_HLSL_BIT,

	SPVC_COMPILER_OPTION_MSL_IOS_USE_SIMDGROUP_FUNCTIONS = 72 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_EMULATE_SUBGROUPS = 73 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_FIXED_SUBGROUP_SIZE = 74 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_FORCE_SAMPLE_RATE_SHADING = 75 | SPVC_COMPILER_OPTION_MSL_BIT,
	SPVC_COMPILER_OPTION_MSL_IOS_SUPPORT_BASE_VERTEX_INSTANCE = 76 | SPVC_COMPILER_OPTION_MSL_BIT,

	SPVC_COMPILER_OPTION_GLSL_OVR_MULTIVIEW_VIEW_COUNT = 77 | SPVC_COMPILER_OPTION_GLSL_BIT,

	SPVC_COMPILER_OPTION_RELAX_NAN_CHECKS = 78 | SPVC_COMPILER_OPTION_COMMON_BIT,

	SPVC_COMPILER_OPTION_INT_MAX = 0x7fffffff
} spvc_compiler_option;

typedef enum SpvDecoration_ {
    SpvDecorationRelaxedPrecision = 0,
    SpvDecorationSpecId = 1,
    SpvDecorationBlock = 2,
    SpvDecorationBufferBlock = 3,
    SpvDecorationRowMajor = 4,
    SpvDecorationColMajor = 5,
    SpvDecorationArrayStride = 6,
    SpvDecorationMatrixStride = 7,
    SpvDecorationGLSLShared = 8,
    SpvDecorationGLSLPacked = 9,
    SpvDecorationCPacked = 10,
    SpvDecorationBuiltIn = 11,
    SpvDecorationNoPerspective = 13,
    SpvDecorationFlat = 14,
    SpvDecorationPatch = 15,
    SpvDecorationCentroid = 16,
    SpvDecorationSample = 17,
    SpvDecorationInvariant = 18,
    SpvDecorationRestrict = 19,
    SpvDecorationAliased = 20,
    SpvDecorationVolatile = 21,
    SpvDecorationConstant = 22,
    SpvDecorationCoherent = 23,
    SpvDecorationNonWritable = 24,
    SpvDecorationNonReadable = 25,
    SpvDecorationUniform = 26,
    SpvDecorationUniformId = 27,
    SpvDecorationSaturatedConversion = 28,
    SpvDecorationStream = 29,
    SpvDecorationLocation = 30,
    SpvDecorationComponent = 31,
    SpvDecorationIndex = 32,
    SpvDecorationBinding = 33,
    SpvDecorationDescriptorSet = 34,
    SpvDecorationOffset = 35,
    SpvDecorationXfbBuffer = 36,
    SpvDecorationXfbStride = 37,
    SpvDecorationFuncParamAttr = 38,
    SpvDecorationFPRoundingMode = 39,
    SpvDecorationFPFastMathMode = 40,
    SpvDecorationLinkageAttributes = 41,
    SpvDecorationNoContraction = 42,
    SpvDecorationInputAttachmentIndex = 43,
    SpvDecorationAlignment = 44,
    SpvDecorationMaxByteOffset = 45,
    SpvDecorationAlignmentId = 46,
    SpvDecorationMaxByteOffsetId = 47,
    SpvDecorationNoSignedWrap = 4469,
    SpvDecorationNoUnsignedWrap = 4470,
    SpvDecorationExplicitInterpAMD = 4999,
    SpvDecorationOverrideCoverageNV = 5248,
    SpvDecorationPassthroughNV = 5250,
    SpvDecorationViewportRelativeNV = 5252,
    SpvDecorationSecondaryViewportRelativeNV = 5256,
    SpvDecorationPerPrimitiveNV = 5271,
    SpvDecorationPerViewNV = 5272,
    SpvDecorationPerTaskNV = 5273,
    SpvDecorationPerVertexKHR = 5285,
    SpvDecorationPerVertexNV = 5285,
    SpvDecorationNonUniform = 5300,
    SpvDecorationNonUniformEXT = 5300,
    SpvDecorationRestrictPointer = 5355,
    SpvDecorationRestrictPointerEXT = 5355,
    SpvDecorationAliasedPointer = 5356,
    SpvDecorationAliasedPointerEXT = 5356,
    SpvDecorationBindlessSamplerNV = 5398,
    SpvDecorationBindlessImageNV = 5399,
    SpvDecorationBoundSamplerNV = 5400,
    SpvDecorationBoundImageNV = 5401,
    SpvDecorationSIMTCallINTEL = 5599,
    SpvDecorationReferencedIndirectlyINTEL = 5602,
    SpvDecorationClobberINTEL = 5607,
    SpvDecorationSideEffectsINTEL = 5608,
    SpvDecorationVectorComputeVariableINTEL = 5624,
    SpvDecorationFuncParamIOKindINTEL = 5625,
    SpvDecorationVectorComputeFunctionINTEL = 5626,
    SpvDecorationStackCallINTEL = 5627,
    SpvDecorationGlobalVariableOffsetINTEL = 5628,
    SpvDecorationCounterBuffer = 5634,
    SpvDecorationHlslCounterBufferGOOGLE = 5634,
    SpvDecorationHlslSemanticGOOGLE = 5635,
    SpvDecorationUserSemantic = 5635,
    SpvDecorationUserTypeGOOGLE = 5636,
    SpvDecorationFunctionRoundingModeINTEL = 5822,
    SpvDecorationFunctionDenormModeINTEL = 5823,
    SpvDecorationRegisterINTEL = 5825,
    SpvDecorationMemoryINTEL = 5826,
    SpvDecorationNumbanksINTEL = 5827,
    SpvDecorationBankwidthINTEL = 5828,
    SpvDecorationMaxPrivateCopiesINTEL = 5829,
    SpvDecorationSinglepumpINTEL = 5830,
    SpvDecorationDoublepumpINTEL = 5831,
    SpvDecorationMaxReplicatesINTEL = 5832,
    SpvDecorationSimpleDualPortINTEL = 5833,
    SpvDecorationMergeINTEL = 5834,
    SpvDecorationBankBitsINTEL = 5835,
    SpvDecorationForcePow2DepthINTEL = 5836,
    SpvDecorationBurstCoalesceINTEL = 5899,
    SpvDecorationCacheSizeINTEL = 5900,
    SpvDecorationDontStaticallyCoalesceINTEL = 5901,
    SpvDecorationPrefetchINTEL = 5902,
    SpvDecorationStallEnableINTEL = 5905,
    SpvDecorationFuseLoopsInFunctionINTEL = 5907,
    SpvDecorationBufferLocationINTEL = 5921,
    SpvDecorationIOPipeStorageINTEL = 5944,
    SpvDecorationFunctionFloatingPointModeINTEL = 6080,
    SpvDecorationSingleElementVectorINTEL = 6085,
    SpvDecorationVectorComputeCallableFunctionINTEL = 6087,
    SpvDecorationMediaBlockIOINTEL = 6140,
    SpvDecorationMax = 0x7fffffff,
} SpvDecoration;

/* Get notified in a callback when an error triggers. Useful for debugging. */
typedef void (*spvc_error_callback)(void* userdata, const char* error);

//This part of the header was modified by Kubagliko_PL

#include <functional>

extern std::function<spvc_result(spvc_context* context)> spvc_context_create;
extern std::function<void(spvc_context context)> spvc_context_destroy;
extern std::function<void(spvc_context context, spvc_error_callback cb, void* userdata)> spvc_context_set_error_callback;

extern std::function<spvc_result(spvc_context context, const SpvId* spirv, size_t word_count, spvc_parsed_ir* parsed_ir)> spvc_context_parse_spirv;
extern std::function<spvc_result(spvc_context context, spvc_backend backend, spvc_parsed_ir parsed_ir, spvc_capture_mode mode, spvc_compiler* compiler)> spvc_context_create_compiler;

extern std::function<spvc_result(spvc_compiler compiler, spvc_resources* resources)> spvc_compiler_create_shader_resources;
extern std::function<spvc_result(spvc_resources resources, spvc_resource_type type, const spvc_reflected_resource** resource_list, size_t* resource_size)> spvc_resources_get_resource_list_for_type;

extern std::function<void(spvc_compiler compiler, SpvId id, SpvDecoration decoration, unsigned argument)> spvc_compiler_set_decoration;
extern std::function<void(spvc_compiler compiler, SpvId id, SpvDecoration decoration, const char* argument)> spvc_compiler_set_decoration_string;
extern std::function<void(spvc_compiler compiler, SpvId id, const char* argument)> spvc_compiler_set_name;
extern std::function<void(spvc_compiler compiler, SpvId id, SpvDecoration decoration)> spvc_compiler_unset_decoration;
extern std::function<const char* (spvc_compiler compiler, SpvId id)> spvc_compiler_get_name;
extern std::function<unsigned(spvc_compiler compiler, SpvId id, SpvDecoration decoration)> spvc_compiler_get_decoration;
extern std::function<const char* (spvc_compiler compiler, SpvId id, SpvDecoration decoration)> spvc_compiler_get_decoration_string;

extern std::function<spvc_result(spvc_compiler compiler, spvc_compiler_options* options)> spvc_compiler_create_compiler_options;
extern std::function<spvc_result(spvc_compiler_options options, spvc_compiler_option option, spvc_bool value)> spvc_compiler_options_set_bool;
extern std::function<spvc_result(spvc_compiler_options options, spvc_compiler_option option, unsigned  value)> spvc_compiler_options_set_uint;
extern std::function<spvc_result(spvc_compiler compiler, spvc_compiler_options options)> spvc_compiler_install_compiler_options;

extern std::function<spvc_result(spvc_compiler compiler, const char* name, SpvExecutionModel model)> spvc_compiler_set_entry_point;
extern std::function<spvc_result(spvc_compiler compiler, const char** source)> spvc_compiler_compile;

extern std::function<spvc_result(spvc_compiler compilerl)> spvc_compiler_build_combined_image_samplers;
extern std::function<spvc_result(spvc_compiler compiler, const spvc_combined_image_sampler** samplers, size_t* num_samplers)> spvc_compiler_get_combined_image_samplers;

#endif