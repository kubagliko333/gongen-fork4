#pragma once

#include <string>
#include <tinyxml2.h>
#include <ft2build.h>
#include <freetype/freetype.h>
#include <vector>
#include <gongen/gcf/gcf.hpp>
#include <msdfgen.h>

namespace gent {
	struct GlyphInfo {
		uint32_t codepoint;

		

		double advanceX;
		double advanceY;

		uint32_t glyphW;
		uint32_t glyphH;
		void* data;
	};

	class FontBuilder {
		FT_Face face = nullptr;

		std::vector<GlyphInfo> glyphs;

		std::string name;

		double emSize;
		double ascenderY;
		double descenderY;
		double lineHeight;
		double underlineY;
		double underlineThickness;

		void generateTexture(gen::GCFHandle& handle, tinyxml2::XMLElement* pGCF_Node, tinyxml2::XMLElement* pNode);
	public:
		~FontBuilder();
		void load(const std::string& filepath, const std::string& name);
		void addGlyphRange(uint32_t start, uint32_t end);
		void addGlyph(uint32_t codepoint);
		void save(gen::GCFHandle& handle, tinyxml2::XMLElement* pGCF_Node, tinyxml2::XMLElement* pNode);
	};
}
