#pragma once

#include <gongen/core/core.hpp>
#include <gongen/window_api/window.hpp>
#include <gongen/gcf/gcf.hpp>
#include <gongen/graphics_api/graphics.hpp>
#include <gongen/imgui/imgui.hpp>
#include "editor.hpp"
#include "render.hpp"

namespace genvec {
	struct Context {
		gen::WindowAPI* windowAPI;
		gen::Window* window;

		gen::GraphicsAPI* graphicsAPI;
		gen::GraphicsDevice* graphicsDevice;
		gen::Swapchain* swapchain;
		gen::CommandBufferPtr commandBuffer;

		uint64_t selectedPoint = 0;
		genvec::object root;

		gen::VectorPartRenderer renderer;
	};
}
