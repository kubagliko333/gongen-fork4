#include <genTools.hpp>
#include <vector>
#include <string>
#include <stdio.h>

void loadAndProcessXMLTaskList(const std::string& filename);
std::string loadTextFile(const std::string& filename);

gent::Context context;

extern const char* shaderc_name;
extern const char* spirv_cross_name;

void gent::initContext(Context* context) {
	//Init Context
	if (!loadSPIRVCross()) {
		throw gent::exception("Can't load: " + std::string(spirv_cross_name));
	}
	bool shadercLoaded = loadShaderc();
	bool dxcompilerLoaded = false;
	if (!shadercLoaded && !dxcompilerLoaded) {
		throw gent::exception("Can't load: " + std::string(shaderc_name));
	}
	if (shadercLoaded) {
		context->shaderCompiler = new gent::internal::ShadercShaderCompiler();
	}
}

int main(int argc, char *argv[]) {
	//Get Args
	std::vector<std::string> args;
	for (uint32_t i = 1; i < argc; i++) {
		args.push_back(std::string(argv[i]));
	}
	try {
		gent::initContext(&context);

		if (args.empty()) {
			loadAndProcessXMLTaskList("data/test.xml");
		}
		else {
			loadAndProcessXMLTaskList(args[0]);
		}

		context.cache.save();
	} catch (gent::exception& e) {
		printf("GenTools error: %s\n", e.message.c_str());
		//getchar();
	} catch (std::exception& e) {
		printf("Standard error: %s\n", e.what());
		//getchar();
	}

	return 0;
}

#include <tinyxml2.h>

extern void processXMLTaskList(gent::Context* context, tinyxml2::XMLDocument &xmlDoc);

void loadAndProcessXMLTaskList(const std::string& filename) {
	std::string str = loadTextFile(filename);
	tinyxml2::XMLDocument xmlDoc;
	if (xmlDoc.Parse(str.c_str(), str.size()) != tinyxml2::XML_SUCCESS) {
		throw gent::exception(std::string("Can't parse XML: " + filename).c_str());
	}
	processXMLTaskList(&context, xmlDoc);
}

#include <gongen/core/file.hpp>

std::string loadTextFile(const std::string& filename) {
	gen::FileHandle fhandle(filename.c_str(), gen::FileOpenMode::READ);
	if (!fhandle.isOpen()) {
		throw gent::exception(std::string("Can't open file: " + filename).c_str());
	}
	std::string str;
	str.resize(fhandle.size());
	fhandle.read(str.size(), str.data());
	return str;
}
