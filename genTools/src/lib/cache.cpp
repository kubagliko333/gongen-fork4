#include <cache.hpp>
#define STB_SPRINTF_IMPLEMENTATION
#include <stb_sprintf.h>

std::string getEntryFilename(gent::CacheEntry* entry, std::string name) {
	std::string out = name + "_";

	char buffer[sizeof(unsigned long) * 8 + 1];

	stbsp_sprintf(buffer, "%llu", entry->hash.data[0]);
	out += std::string(buffer);
	out += "_";

	stbsp_sprintf(buffer, "%llu", entry->hash.data[1]);
	out += std::string(buffer);

	//Only for testing
	out += ".png";

	return out;
}

#include <filesystem>

gent::Cache::Cache(const std::string& dir) {
	this->dir = dir;

	if (!std::filesystem::exists(dir)) {
		std::filesystem::create_directory(dir);
	}

	if (xmlDoc.LoadFile((dir + "/cache.xml").c_str()) == tinyxml2::XML_SUCCESS) {
		tinyxml2::XMLElement* pRoot = xmlDoc.FirstChildElement("GenTools_Cache");

		tinyxml2::XMLElement* pEntry = pRoot->FirstChildElement();
		while (pEntry) {
			std::string name = pEntry->Attribute("name");
			std::string filename = pEntry->Attribute("filename");
			gen::Hash128 hash;
			hash.data[0] = pEntry->Unsigned64Attribute("hash0");
			hash.data[1] = pEntry->Unsigned64Attribute("hash1");

			entries[name].filename = filename;
			entries[name].hash = hash;

			pEntry = pEntry->NextSiblingElement();
		}
	}
}

#include <gongen/core/file.hpp>

void gent::Cache::save() {
	xmlDoc.Clear();
	tinyxml2::XMLElement* pRoot = xmlDoc.NewElement("GenTools_Cache");

	for (auto& e : entries) {
		tinyxml2::XMLElement* pEntry = pRoot->InsertNewChildElement("Entry");
		pEntry->SetAttribute("name", e.first.c_str());
		pEntry->SetAttribute("filename", e.second.filename.c_str());
		pEntry->SetAttribute("hash0", e.second.hash.data[0]);
		pEntry->SetAttribute("hash1", e.second.hash.data[1]);
	}

	xmlDoc.InsertEndChild(pRoot);

	gen::FileHandle file((dir + "/cache.xml").c_str(), gen::FileOpenMode::WRITE);

	tinyxml2::XMLPrinter printer(nullptr);
	xmlDoc.Print(&printer);

	file.write((printer.CStrSize() - 1), (void*)printer.CStr());
}

void gent::Cache::addEntry(std::string name, gen::Hash128 hash, void* data, uint64_t dataSize) {
	entries[name].hash = hash;
	entries[name].filename = getEntryFilename(&entries[name], name);

	gen::FileHandle file((dir + "/" + entries[name].filename).c_str(), gen::FileOpenMode::WRITE);
	file.write(dataSize, data);
}

bool gent::Cache::checkEntry(std::string name, gen::Hash128 hash) {
	if (entries.find(name) == entries.end()) {
		return false;
	}
	else {
		gent::CacheEntry* entry = &entries[name];
		if (entry->hash == hash) {
			return true;
		}
		else {
			return false;
		}
	}
}

void* gent::Cache::getEntryData(std::string name, gen::Hash128 hash, uint64_t& dataSize) {
	if (checkEntry(name, hash)) {
		gen::FileHandle file((dir + "/" + entries[name].filename).c_str(), gen::FileOpenMode::READ);
		dataSize = file.size();
		void* data = malloc(dataSize);
		file.read(dataSize, data);
		return data;
	}
	else {
		return nullptr;
	}
}
