#include <shaderCompiler.hpp>
#include <genTools.hpp>

void spvcErrorCallback(void* userData, const char* text) {
	throw gent::exception(std::string("SPIRV-Cross Error: " + std::string(text)).c_str());
}

gent::internal::ShadercShaderCompiler::ShadercShaderCompiler() {
	compiler = shaderc_compiler_initialize();
	spvc_context_create(&context);
	spvc_context_set_error_callback(context, spvcErrorCallback, nullptr);
	initSPIRVCross();
}

gent::internal::ShadercShaderCompiler::~ShadercShaderCompiler() {
}

std::vector<uint32_t> gent::internal::ShadercShaderCompiler::compileSPIRV(const std::string& code, const std::string& filename, gen::ShaderStage type, std::vector<std::pair<std::string, std::string>> macros) {
	if (compileOptions) shaderc_compile_options_release(compileOptions);
	compileOptions = shaderc_compile_options_initialize();
	shaderc_compile_options_set_optimization_level(compileOptions, shaderc_optimization_level_performance);
	shaderc_compile_options_set_source_language(compileOptions, shaderc_source_language_hlsl);
	shaderc_compile_options_set_target_env(compileOptions, shaderc_target_env_vulkan, shaderc_env_version_vulkan_1_0);
	shaderc_compile_options_set_target_spirv(compileOptions, shaderc_spirv_version_1_0);

	for (auto& m : macros) {
		shaderc_compile_options_add_macro_definition(compileOptions, m.first.c_str(), m.first.size(), m.second.c_str(), m.second.size());
	}

	const char* entryPoint = "";
	shaderc_shader_kind shaderType;
	if (type == gen::ShaderStage::VERTEX_SHADER) {
		entryPoint = "vertexShader";
		shaderType = shaderc_shader_kind::shaderc_vertex_shader;
	}
	else if (type == gen::ShaderStage::HULL_SHADER) {
		entryPoint = "hullShader";
		shaderType = shaderc_shader_kind::shaderc_tess_control_shader;
	}
	else if (type == gen::ShaderStage::DOMAIN_SHADER) {
		entryPoint = "domainShader";
		shaderType = shaderc_shader_kind::shaderc_tess_evaluation_shader;
	}
	else if (type == gen::ShaderStage::GEOMETRY_SHADER) {
		entryPoint = "geometryShader";
		shaderType = shaderc_shader_kind::shaderc_geometry_shader;
	}
	else if (type == gen::ShaderStage::PIXEL_SHADER) {
		entryPoint = "pixelShader";
		shaderType = shaderc_shader_kind::shaderc_fragment_shader;
	}
	else if (type == gen::ShaderStage::COMPUTE_SHADER) {
		entryPoint = "computeShader";
		shaderType = shaderc_shader_kind::shaderc_compute_shader;
	}

	shaderc_compilation_result_t result = shaderc_compile_into_spv(compiler, code.c_str(), code.size(), shaderType, filename.c_str(), entryPoint, compileOptions);
	shaderc_compilation_status status = shaderc_result_get_compilation_status(result);
	std::vector<uint32_t> out;
	if (status != shaderc_compilation_status_success) {
		std::string error = std::string(shaderc_result_get_error_message(result));
		shaderc_result_release(result);
		throw gent::exception(("Shader compilation error: " + error).c_str());
	}
	else {
		size_t dataSize = shaderc_result_get_length(result);
		out.resize(dataSize / sizeof(uint32_t));
		const char* data = shaderc_result_get_bytes(result);
		memcpy(out.data(), data, dataSize);
		shaderc_result_release(result);
	}
	return out;
}

extern std::string loadTextFile(const std::string& filename);

std::vector<uint32_t> gent::internal::ShadercShaderCompiler::compileSPIRV(const std::string& filename, gen::ShaderStage type, std::vector<std::pair<std::string, std::string>> macros) {
	std::string code = loadTextFile(filename);
	return compileSPIRV(code, filename, type, macros);
}

void setupSPIRVMetadata(spvc_compiler compiler, spvc_resources resources, gen::ShaderMetadata& metadata, spvc_resource_type type, gen::SPIRVDescriptorType genType) {
	const spvc_reflected_resource* resource;
	size_t resourceAmount;
	spvc_resources_get_resource_list_for_type(resources, type, &resource, &resourceAmount);
	for (uint32_t i = 0; i < resourceAmount; i++) {
		uint32_t set = spvc_compiler_get_decoration(compiler, resource[i].id, SpvDecorationDescriptorSet);
		uint32_t binding = spvc_compiler_get_decoration(compiler, resource[i].id, SpvDecorationBinding);
		metadata.descriptors[set][binding] = genType;
	}
}

gent::internal::IShaderCompiler::IShaderCompiler() {}

gen::ShaderMetadata gent::internal::IShaderCompiler::reflectSPIRV(std::vector<uint32_t>&spirv) {
	spvc_parsed_ir ir;
	spvc_resources resources;
	spvc_compiler compiler;

	spvc_context_parse_spirv(context, spirv.data(), spirv.size(), &ir);
	spvc_context_create_compiler(context, SPVC_BACKEND_NONE, ir, SPVC_CAPTURE_MODE_TAKE_OWNERSHIP, &compiler);
	spvc_compiler_create_shader_resources(compiler, &resources);

	gen::ShaderMetadata metadata;
	setupSPIRVMetadata(compiler, resources, metadata, SPVC_RESOURCE_TYPE_SEPARATE_SAMPLERS, gen::SPIRVDescriptorType::SAMPLER);
	setupSPIRVMetadata(compiler, resources, metadata, SPVC_RESOURCE_TYPE_SEPARATE_IMAGE, gen::SPIRVDescriptorType::SEPARATE_IMAGE);
	setupSPIRVMetadata(compiler, resources, metadata, SPVC_RESOURCE_TYPE_STORAGE_IMAGE, gen::SPIRVDescriptorType::STORAGE_IMAGE);
	setupSPIRVMetadata(compiler, resources, metadata, SPVC_RESOURCE_TYPE_UNIFORM_BUFFER, gen::SPIRVDescriptorType::UNIFORM_BUFFER);
	setupSPIRVMetadata(compiler, resources, metadata, SPVC_RESOURCE_TYPE_STORAGE_BUFFER, gen::SPIRVDescriptorType::STORAGE_BUFFER);

	return metadata;
}

void gent::internal::IShaderCompiler::initSPIRVCross() {
	spvc_context_create(&context);
	spvc_context_set_error_callback(context, spvcErrorCallback, nullptr);
}

void remapBindings(gent::ShaderVariant *variant, spvc_compiler compiler, spvc_resources resources, gen::ShaderMetadata* metadata, spvc_resource_type type, gen::ShaderType shaderType) {
	const spvc_reflected_resource* resource;
	size_t resourceAmount;
	spvc_resources_get_resource_list_for_type(resources, type, &resource, &resourceAmount);
	for (uint32_t i = 0; i < resourceAmount; i++) {
		uint32_t set = spvc_compiler_get_decoration(compiler, resource[i].id, SpvDecorationDescriptorSet);
		uint32_t binding = spvc_compiler_get_decoration(compiler, resource[i].id, SpvDecorationBinding);

		std::atomic<uint32_t>* remapIndex = nullptr;
		uint32_t remapped = 0;

		if ((shaderType == gen::ShaderType::GLSL330) || (shaderType == gen::ShaderType::ELSL300)) {
			gent::BindingRemapContext* remapContext = nullptr;
			if (shaderType == gen::ShaderType::GLSL330) remapContext = &variant->glslRemapContext;
			else if (shaderType == gen::ShaderType::ELSL300) remapContext = &variant->elslRemapContext;

			if (type == SPVC_RESOURCE_TYPE_SEPARATE_SAMPLERS) remapIndex = &remapContext->nextSampler;
			else if (type == SPVC_RESOURCE_TYPE_SEPARATE_IMAGE) remapIndex = &remapContext->nextTexture;
			else if (type == SPVC_RESOURCE_TYPE_STORAGE_IMAGE) remapIndex = &remapContext->nextImage;
			else if (type == SPVC_RESOURCE_TYPE_UNIFORM_BUFFER) remapIndex = &remapContext->nextBuffer;
			else if (type == SPVC_RESOURCE_TYPE_STORAGE_BUFFER) remapIndex = &remapContext->nextBuffer;
			
			remapped = remapIndex->load();
			remapIndex->fetch_add(1);
		}

		metadata->remapTable[set][binding] = remapped;

		spvc_compiler_unset_decoration(compiler, resource[i].id, SpvDecorationDescriptorSet);
		if ((type == SPVC_RESOURCE_TYPE_SEPARATE_SAMPLERS) || (type == SPVC_RESOURCE_TYPE_SEPARATE_IMAGE)) {

		}
		spvc_compiler_unset_decoration(compiler, resource[i].id, SpvDecorationBinding);

		spvc_compiler_set_decoration(compiler, resource[i].id, SpvDecorationBinding, remapped);

		metadata->descriptorSPIRVIndexes[set][binding] = resource[i].id;
	}
}

std::string gent::internal::IShaderCompiler::compileGLSL(uint32_t stage, gent::ShaderVariant* variant, gen::ShaderMetadata* metadata, const std::string& code, const std::string& filename, std::vector<uint32_t>& spirv, bool onlyReflect, gen::ShaderType type) {
	spvc_parsed_ir ir;
	spvc_resources resources;
	spvc_compiler compiler;

	spvc_context_parse_spirv(context, spirv.data(), spirv.size(), &ir);
	spvc_context_create_compiler(context, SPVC_BACKEND_GLSL, ir, SPVC_CAPTURE_MODE_TAKE_OWNERSHIP, &compiler);
	spvc_compiler_create_shader_resources(compiler, &resources);

	//Remapping bindings
	remapBindings(variant, compiler, resources, metadata, SPVC_RESOURCE_TYPE_SEPARATE_SAMPLERS, type);
	remapBindings(variant, compiler, resources, metadata, SPVC_RESOURCE_TYPE_SEPARATE_IMAGE, type);
	remapBindings(variant, compiler, resources, metadata, SPVC_RESOURCE_TYPE_STORAGE_IMAGE, type);
	remapBindings(variant, compiler, resources, metadata, SPVC_RESOURCE_TYPE_UNIFORM_BUFFER, type);
	remapBindings(variant, compiler, resources, metadata, SPVC_RESOURCE_TYPE_STORAGE_BUFFER, type);
	
	//Setuping combined textures
	gent::BindingRemapContext* remapContext = nullptr;
	if (type == gen::ShaderType::GLSL330) remapContext = &variant->glslRemapContext;
	else if (type == gen::ShaderType::ELSL300) remapContext = &variant->elslRemapContext;

	spvc_compiler_build_combined_image_samplers(compiler);
	const spvc_combined_image_sampler* combinedImageSampler;
	size_t combinedImageSamplers;
	spvc_compiler_get_combined_image_samplers(compiler, &combinedImageSampler, &combinedImageSamplers);
	for (uint32_t i = 0; i < combinedImageSamplers; i++) {
		uint32_t imageBinding = spvc_compiler_get_decoration(compiler, combinedImageSampler[i].image_id, SpvDecorationBinding);
		uint32_t samplerBinding = spvc_compiler_get_decoration(compiler, combinedImageSampler[i].sampler_id, SpvDecorationBinding);

		std::atomic<uint32_t>* remapIndex = &remapContext->nextTexture2;

		uint32_t imageBinding2 = remapIndex->load();
		remapIndex->fetch_add(1);

		spvc_compiler_set_decoration(compiler, combinedImageSampler[i].combined_id, SpvDecorationBinding, imageBinding2);

		gen::CombinedImageSamplerMetadata combined;
		combined.imageBinding = imageBinding;
		combined.samplerBinding = samplerBinding;
		combined.spirvIndex = combinedImageSampler[i].combined_id;

		metadata->textureCombiningTable[imageBinding2] = combined;
	}

	const spvc_reflected_resource* resource;
	size_t resourceAmount;

	if (stage != gen::ShaderStageFlag::COMPUTE_SHADER) {
		if (stage != gen::ShaderStageFlag::VERTEX_SHADER) {
			spvc_resources_get_resource_list_for_type(resources, SPVC_RESOURCE_TYPE_STAGE_INPUT, &resource, &resourceAmount);
			for (uint32_t i = 0; i < resourceAmount; i++) {
				uint32_t binding = spvc_compiler_get_decoration(compiler, resource[i].id, SpvDecorationBinding);

				std::string name = "GEN_V_" + std::to_string(binding);

				spvc_compiler_set_name(compiler, resource[i].id, name.c_str());
			}
		}

		if (stage != gen::ShaderStageFlag::PIXEL_SHADER) {
			spvc_resources_get_resource_list_for_type(resources, SPVC_RESOURCE_TYPE_STAGE_OUTPUT, &resource, &resourceAmount);
			for (uint32_t i = 0; i < resourceAmount; i++) {
				uint32_t binding = spvc_compiler_get_decoration(compiler, resource[i].id, SpvDecorationBinding);

				std::string name = "GEN_V_" + std::to_string(binding);

				spvc_compiler_set_name(compiler, resource[i].id, name.c_str());
			}
		}
	}

	//Compiling to GLSL
	if (!onlyReflect) {
		spvc_compiler_options options;
		spvc_compiler_create_compiler_options(compiler, &options);
		
		spvc_compiler_options_set_bool(options, SPVC_COMPILER_OPTION_GLSL_SUPPORT_NONZERO_BASE_INSTANCE, true);
		spvc_compiler_options_set_bool(options, SPVC_COMPILER_OPTION_GLSL_SEPARATE_SHADER_OBJECTS, false);
		spvc_compiler_options_set_bool(options, SPVC_COMPILER_OPTION_GLSL_ENABLE_420PACK_EXTENSION, false);
		spvc_compiler_options_set_bool(options, SPVC_COMPILER_OPTION_GLSL_VULKAN_SEMANTICS, false);
		spvc_compiler_options_set_bool(options, SPVC_COMPILER_OPTION_GLSL_ES_DEFAULT_FLOAT_PRECISION_HIGHP, true);
		spvc_compiler_options_set_bool(options, SPVC_COMPILER_OPTION_GLSL_ES_DEFAULT_INT_PRECISION_HIGHP, true);
		spvc_compiler_options_set_bool(options, SPVC_COMPILER_OPTION_FORCE_TEMPORARY, false);
		spvc_compiler_options_set_bool(options, SPVC_COMPILER_OPTION_FLATTEN_MULTIDIMENSIONAL_ARRAYS, false);
		spvc_compiler_options_set_bool(options, SPVC_COMPILER_OPTION_FIXUP_DEPTH_CONVENTION, false);
		spvc_compiler_options_set_bool(options, SPVC_COMPILER_OPTION_FLIP_VERTEX_Y, false);
		spvc_compiler_options_set_bool(options, SPVC_COMPILER_OPTION_GLSL_EMIT_PUSH_CONSTANT_AS_UNIFORM_BUFFER, false);
		spvc_compiler_options_set_bool(options, SPVC_COMPILER_OPTION_GLSL_EMIT_UNIFORM_BUFFER_AS_PLAIN_UNIFORMS, false);

		bool elsl = (type == gen::ShaderType::ELSL300);
		if (elsl) {
			spvc_compiler_options_set_bool(options, SPVC_COMPILER_OPTION_GLSL_ES, true);
			spvc_compiler_options_set_uint(options, SPVC_COMPILER_OPTION_GLSL_VERSION, 300);
		}
		else {
			spvc_compiler_options_set_bool(options, SPVC_COMPILER_OPTION_GLSL_ES, false);
			spvc_compiler_options_set_uint(options, SPVC_COMPILER_OPTION_GLSL_VERSION, 330);
		}
		
		spvc_compiler_install_compiler_options(compiler, options);

		const char* code;
		spvc_compiler_compile(compiler, &code);
		return std::string(code);
	}
	else {
		return "";
	}
}
