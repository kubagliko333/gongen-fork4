#include <font.hpp>
#include <genTools.hpp>
#include <filesystem>

bool freetypeInitialized = false;
FT_Library library;

void gent::FontBuilder::load(const std::string& filepath, const std::string& name) {
	if (!freetypeInitialized) {
		FT_Error e = FT_Init_FreeType(&library);
		freetypeInitialized = true;
		if (e != FT_Err_Ok) {
			throw gent::exception("Can't init FreeType: " + std::to_string(e));
		}
	}

	FT_Error e = FT_New_Face(library, filepath.c_str(), 0, &face);
	if (e != FT_Err_Ok) {
		throw gent::exception("Can't load font: " + filepath);
	}

	this->name = name + std::filesystem::path(filepath).filename().string();

	//TODO LOAD KERNING
}

gent::FontBuilder::~FontBuilder() {
	if (face) FT_Done_Face(face);
}

void gent::FontBuilder::addGlyphRange(uint32_t start, uint32_t end) {
	for (uint32_t i = start; i < (end + 1); i++) {
		addGlyph(i);
	}
}

#define F26DOT6_TO_DOUBLE(x) (1/64.*double(x))

extern gent::Context context;

struct FtContext {
	msdfgen::Point2 position;
	msdfgen::Shape* shape;
	msdfgen::Contour* contour;
};

static msdfgen::Point2 ftPoint2(const FT_Vector& vector) {
	return msdfgen::Point2(F26DOT6_TO_DOUBLE(vector.x), F26DOT6_TO_DOUBLE(vector.y));
}

static int ftMoveTo(const FT_Vector* to, void* user) {
	FtContext* context = reinterpret_cast<FtContext*>(user);
	if (!(context->contour && context->contour->edges.empty()))
		context->contour = &context->shape->addContour();
	context->position = ftPoint2(*to);
	return 0;
}

static int ftLineTo(const FT_Vector* to, void* user) {
	FtContext* context = reinterpret_cast<FtContext*>(user);
	msdfgen::Point2 endpoint = ftPoint2(*to);
	if (endpoint != context->position) {
		context->contour->addEdge(new msdfgen::LinearSegment(context->position, endpoint));
		context->position = endpoint;
	}
	return 0;
}

static int ftConicTo(const FT_Vector* control, const FT_Vector* to, void* user) {
	FtContext* context = reinterpret_cast<FtContext*>(user);
	context->contour->addEdge(new msdfgen::QuadraticSegment(context->position, ftPoint2(*control), ftPoint2(*to)));
	context->position = ftPoint2(*to);
	return 0;
}

static int ftCubicTo(const FT_Vector* control1, const FT_Vector* control2, const FT_Vector* to, void* user) {
	FtContext* context = reinterpret_cast<FtContext*>(user);
	context->contour->addEdge(new msdfgen::CubicSegment(context->position, ftPoint2(*control1), ftPoint2(*control2), ftPoint2(*to)));
	context->position = ftPoint2(*to);
	return 0;
}

#include FT_OUTLINE_H
#define GLYPH_SIZE 16

void gent::FontBuilder::addGlyph(uint32_t codepoint) {
	FT_Error e = FT_Load_Char(face, codepoint, FT_LOAD_NO_SCALE);
	if (e == FT_Err_Ok) {

	}

	std::string cacheName = name + "_" + std::to_string(codepoint);
	gen::Hash128 hash;
	if (context.cache.checkEntry(cacheName, hash)) {

	}
	else {
		gent::GlyphInfo glyph;
		glyph.codepoint = codepoint;
		//GET METRICS
		/*glyph.emSize = F26DOT6_TO_DOUBLE(face->units_per_EM);
		glyph.emSize = F26DOT6_TO_DOUBLE(face->units_per_EM);
		glyph.emSize = F26DOT6_TO_DOUBLE(face->units_per_EM);
		glyph.emSize = F26DOT6_TO_DOUBLE(face->units_per_EM);
		glyph.emSize = F26DOT6_TO_DOUBLE(face->units_per_EM);
		glyph.emSize = F26DOT6_TO_DOUBLE(face->units_per_EM);*/
	

		//MSDFGEN GENERATION
		msdfgen::Shape output;

		FtContext context = { };
		context.shape = &output;
		FT_Outline_Funcs ftFunctions;
		ftFunctions.move_to = &ftMoveTo;
		ftFunctions.line_to = &ftLineTo;
		ftFunctions.conic_to = &ftConicTo;
		ftFunctions.cubic_to = &ftCubicTo;
		ftFunctions.shift = 0;
		ftFunctions.delta = 0;
		e = FT_Outline_Decompose(&face->glyph->outline, &ftFunctions, &context);

		/*glyph.bitmap = msdfgen::Bitmap<float, 4>(GLYPH_SIZE, GLYPH_SIZE);
		msdfgen::edgeColoringSimple(output, 3.0);
		msdfgen::generateMTSDF(glyph.bitmap, output, 4.0, 1.0, msdfgen::Vector2(4.0, 4.0));*/
	}
}

void gent::FontBuilder::save(gen::GCFHandle& handle, tinyxml2::XMLElement* pGCF_Node, tinyxml2::XMLElement* pNod) {
	//GENERATING FONT TEXTURE

}
