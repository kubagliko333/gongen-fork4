struct VSInput {
	float2 pos : POSITION;
  float2 uv : TEXCOORD0;
  float4 color : COLOR0;
};

struct PSInput {
	float4 pos : SV_POSITION;
	float2 uv : POSITION;
  float4 color : COLOR0;
};

layout(binding = 0, set = 0) cbuffer UniformBuffer {
	float4x4 projectionMatrix;
};

layout(binding = 1, set = 0) Texture2D texture0 : register(t0);
layout(binding = 2, set = 0) SamplerState sampler0 : register(s0);

PSInput vertexShader(VSInput IN) {
	PSInput OUT;
  OUT.pos = mul(projectionMatrix, float4(IN.pos, 0.0f, 1.0f));
  OUT.uv = IN.uv;
  OUT.color = IN.color;
	return OUT;
}

float4 pixelShader(PSInput IN) : SV_TARGET {
  return IN.color * texture0.Sample(sampler0, IN.uv);
}
