#include "vk_graphics.hpp"

VkSurfaceFormatKHR chooseSwapSurfaceFormat(const std::vector<VkSurfaceFormatKHR>& availableFormats) {
	for (const auto& availableFormat : availableFormats) {
		if (availableFormat.format == VK_FORMAT_R8G8B8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
			return availableFormat;
		}
		if (availableFormat.format == VK_FORMAT_B8G8R8A8_UNORM && availableFormat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
			return availableFormat;
		}
	}
	VkSurfaceFormatKHR out;
	out.format = VK_FORMAT_R8G8B8A8_UNORM;
	out.colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
	return out;
}

VkPresentModeKHR chooseSwapPresentMode(const std::vector<VkPresentModeKHR>& availablePresentModes) {
	return VK_PRESENT_MODE_FIFO_KHR;
}

gen::vkSwapchain::vkSwapchain(gen::Window* window, gen::vkGraphicsDevice* device) {
	this->device = device;
	this->window = window;
	this->waitSemaphore = 0;
	this->swapchain = VK_NULL_HANDLE;
	createSwapchain();
}

void gen::vkSwapchain::createSwapchain() {
	vkDeviceWaitIdle(device->device);

	uint64_t temp = 0;
	surface = (VkSurfaceKHR)window->graphicsFunction(gen::WindowGraphicsFunction::VK_CREATE_SURFACE, (uint64_t)device->instance, 0, 0, temp);

	VkSurfaceCapabilitiesKHR capabilities;
	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device->physicalDevice, surface, &capabilities);

	uint32_t formatCount;
	vkGetPhysicalDeviceSurfaceFormatsKHR(device->physicalDevice, surface, &formatCount, nullptr);
	std::vector<VkSurfaceFormatKHR> formats(formatCount);
	vkGetPhysicalDeviceSurfaceFormatsKHR(device->physicalDevice, surface, &formatCount, formats.data());

	uint32_t presentModeCount;
	vkGetPhysicalDeviceSurfacePresentModesKHR(device->physicalDevice, surface, &presentModeCount, nullptr);
	std::vector<VkPresentModeKHR> presentModes(presentModeCount);
	vkGetPhysicalDeviceSurfacePresentModesKHR(device->physicalDevice, surface, &presentModeCount, presentModes.data());
	
	uint64_t w, h;
	w = window->graphicsFunction(gen::WindowGraphicsFunction::GET_FRAMEBUFFER_SIZE, 0, 0, 0, h);

	this->activeW = w;
	this->activeH = h;
	if ((w == 0) || (h == 0)) {
		minimized = true;
	}
	else {
		minimized = false;

		VkSurfaceFormatKHR surfaceFormat = chooseSwapSurfaceFormat(formats);
		VkPresentModeKHR presentMode = chooseSwapPresentMode(presentModes);
		VkExtent2D extent = { static_cast<uint32_t>(w), static_cast<uint32_t>(h) };

		uint32_t imageCount = capabilities.minImageCount + 1;
		if (capabilities.maxImageCount > 0 && imageCount > capabilities.maxImageCount) {
			imageCount = capabilities.maxImageCount;
		}

		VkSwapchainKHR oldSwapchain = swapchain;

		VkSwapchainCreateInfoKHR createInfo = { VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR, NULL };
		createInfo.flags = 0;
		createInfo.surface = surface;
		createInfo.minImageCount = imageCount;
		createInfo.imageFormat = surfaceFormat.format;
		createInfo.imageColorSpace = surfaceFormat.colorSpace;
		createInfo.imageExtent = extent;
		createInfo.imageArrayLayers = 1;
		createInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
		createInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
		createInfo.queueFamilyIndexCount = 0;
		createInfo.pQueueFamilyIndices = nullptr;
		createInfo.preTransform = capabilities.currentTransform;
		createInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
		createInfo.presentMode = presentMode;
		createInfo.clipped = VK_TRUE;
		createInfo.oldSwapchain = VK_NULL_HANDLE;
		
		VK_ERROR_TEST(vkCreateSwapchainKHR(device->device, &createInfo, nullptr, &swapchain), gen::HardwareException, "Can't Create Vulkan Swapchain")
		std::vector<VkImage> swapChainImages(imageCount);
		vkGetSwapchainImagesKHR(device->device, swapchain, &imageCount, swapChainImages.data());

		//if (oldSwapchain) vkDestroySwapchainKHR(device->device, oldSwapchain, nullptr);

		this->imageAquired = false;
		this->swapchainSemaphore = VK_NULL_HANDLE;
		this->activeImage = 0;
		this->_images.clear();
		this->depthImages.clear();

		for (auto& i : images) {
			gen::vkTexture* vkTexture = (gen::vkTexture*)i.get();
			vkTexture->image = 0;
			vkTexture->imageView = 0;
		}

		this->images.clear();

		this->_images.resize(imageCount);
		this->images.resize(imageCount);
		this->depthImages.resize(imageCount);
		for (uint32_t i = 0; i < imageCount; i++) {
			_images[i].allocation = nullptr;
			_images[i].device = device;
			_images[i].image = swapChainImages[i];
			_images[i].lastFence = 0;
			_images[i].swapchain = this;
			_images[i].swapchainImage = true;
			_images[i].usageFlags = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
			_images[i].textureDesc.bindFlags = gen::TextureBindFlag::TEXTURE_BIND_COLOR_ATTACHMENT;
			_images[i].textureDesc.depth = 1;
			if (surfaceFormat.format == VK_FORMAT_R8G8B8A8_UNORM) _images[i].textureDesc.format = gen::GPUFormat::RGBA8_UNORM;
			else if (surfaceFormat.format == VK_FORMAT_B8G8R8A8_UNORM) _images[i].textureDesc.format = gen::GPUFormat::BGRA8_UNORM;
			_images[i].textureDesc.type = gen::TextureType::TEXTURE_2D;
			_images[i].textureDesc.height = h;
			_images[i].textureDesc.layers = 1;
			_images[i].textureDesc.samples = gen::TextureSamples::SAMPLE_COUNT_1;
			_images[i].textureDesc.mipLevels = 1;
			_images[i].textureDesc.width = w;
			_images[i].samples = VK_SAMPLE_COUNT_1_BIT;

			VkImageViewCreateInfo viewCreateInfo = { VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO, NULL };
			viewCreateInfo.flags = 0;
			viewCreateInfo.image = _images[i].image;
			viewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
			viewCreateInfo.format = surfaceFormat.format;
			viewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
			viewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
			viewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
			viewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
			viewCreateInfo.subresourceRange.baseArrayLayer = 0;
			viewCreateInfo.subresourceRange.baseMipLevel = 0;
			viewCreateInfo.subresourceRange.layerCount = 1;
			viewCreateInfo.subresourceRange.levelCount = 1;
			viewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;

			VK_ERROR_TEST(vkCreateImageView(device->device, &viewCreateInfo, nullptr, &_images[i].imageView), gen::GraphicsResourceCreationException, "Can't Create Swapchain Image View")

			gen::TextureDesc depthTexture;
			depthTexture.format = gen::GPUFormat::D24_UNORM_S8_UINT;
			depthTexture.width = w;
			depthTexture.height = h;
			depthTexture.bindFlags = gen::TextureBindFlag::TEXTURE_BIND_DEPTH_STENCIL_ATTACHMENT;

			depthImages[i] = device->createTexture(depthTexture);
			gen::vkTexture* tx = new gen::vkTexture();
			(*tx) = _images[i];
			images[i] = std::shared_ptr<void>(tx);

			this->renderPass.colorAttachments[0].texture = images[i];
			this->renderPass.colorAttachments[0].loadOp = gen::RenderPassLoadOp::CLEAR;
			this->renderPass.colorAttachments[0].storeOp = gen::RenderPassStoreOp::STORE;
			this->renderPass.colorAttachments[0].clearR = 0.0f;
			this->renderPass.colorAttachments[0].clearG = 0.0f;
			this->renderPass.colorAttachments[0].clearB = 0.0f;
			this->renderPass.colorAttachments[0].clearA = 1.0f;

			this->renderPass.depthStencilAttachment.texture = depthImages[i];
			this->renderPass.depthStencilAttachment.loadOp = gen::RenderPassLoadOp::CLEAR;
			this->renderPass.depthStencilAttachment.storeOp = gen::RenderPassStoreOp::DONT_CARE;
			this->renderPass.depthStencilAttachment.clearD = 0.0f;
		}
		activeImage = 0;
	}
}

void gen::vkSwapchain::aquireImage(VkSemaphore semaphore) {
	if (!imageAquired) {
		VkResult result = vkAcquireNextImageKHR(device->device, swapchain, UINT64_MAX, semaphore, nullptr, &activeImage);
		if (result == VK_ERROR_OUT_OF_DATE_KHR) {
			createSwapchain();
			aquireImage(semaphore);
		}

		renderPass.colorAttachments[0].texture = images[activeImage];
		renderPass.depthStencilAttachment.texture = depthImages[activeImage];
	}
}

void gen::vkSwapchain::nextFrame() {
	imageAquired = false;
}

gen::RenderPassDesc& gen::vkSwapchain::getRenderPass(gen::CommandBuffer* commandBuffer) {
	gen::vkCommandBuffer* vkCommandBuffer = (gen::vkCommandBuffer*)commandBuffer;
	if (!imageAquired) {
		//if (!minimized) {
			aquireImage(vkCommandBuffer->addWaitSemaphore()->semaphore);
			vkCommandBuffer->addSwapchain(this);
		//}
	}
	return renderPass;
}

void gen::vkSwapchain::handleResize(uint32_t x, uint32_t y) {
	if ((activeW != x) || (activeH != y)) {
		createSwapchain();
	}
}
