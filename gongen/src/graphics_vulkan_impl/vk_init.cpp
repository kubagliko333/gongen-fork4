#include "vk_graphics.hpp"
#include <iostream>
#include <string.h>

gen::GraphicsAPI* vulkanCreateGraphicsAPI() {
	return new gen::vkGraphicsAPI();
}

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
	VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
	VkDebugUtilsMessageTypeFlagsEXT messageType,
	const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
	void* pUserData) {
	if ((messageSeverity != VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT) && (messageSeverity != VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT)) {
		std::cout << pCallbackData->pMessage << std::endl;
	}
	return VK_TRUE;
}

std::string gen::vkResultMessage(VkResult result) {
	switch (result) {
		case VK_ERROR_OUT_OF_HOST_MEMORY: return "Out of Host Memory";
		case VK_ERROR_OUT_OF_DEVICE_MEMORY: return "Out of Device Memory";
		case VK_ERROR_INITIALIZATION_FAILED: return "Initialization Failed";
		case VK_ERROR_DEVICE_LOST: return "Device Lost";
		case VK_ERROR_MEMORY_MAP_FAILED: return "Memory Map Failed";
		case VK_ERROR_LAYER_NOT_PRESENT: return "Layer Not Present";
		case VK_ERROR_EXTENSION_NOT_PRESENT: return "Extension Not Present";
		case VK_ERROR_FEATURE_NOT_PRESENT: return "Feature Not Present";
		case VK_ERROR_INCOMPATIBLE_DRIVER:  return "Incompatible Driver";
		case VK_ERROR_TOO_MANY_OBJECTS: return "Too Many Objecys";
		case VK_ERROR_FORMAT_NOT_SUPPORTED: return "Format Not Supported";
		case VK_ERROR_FRAGMENTED_POOL: return "Fragmented Pool";
		case VK_ERROR_SURFACE_LOST_KHR: return "Surface Lost";
		case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR: return "Native Window In Use";
		case VK_ERROR_OUT_OF_DATE_KHR: return "Out of Data";
		case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR: return "Incompatible Display";
		case VK_ERROR_INVALID_SHADER_NV: return "Invalid Shader";
		case VK_ERROR_OUT_OF_POOL_MEMORY: return "Out of Pool Memory";
		case VK_ERROR_INVALID_EXTERNAL_HANDLE: return "Invalid External Handle";
		case VK_ERROR_FRAGMENTATION: return "Fragmentation";
		case VK_ERROR_INVALID_DEVICE_ADDRESS_EXT: return "Invalid Device Address";
		case VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT: return "Full Screen Exclusive Mode Lost";
		case VK_ERROR_COMPRESSION_EXHAUSTED_EXT: return "Compression Exhausted";
		case VK_ERROR_UNKNOWN: return "Unknown";
	}
	return "No Error";
}

void gen::vkGraphicsAPI::init(gen::WindowAPI* windowAPI, GraphicsAPICreateInfo createInfo) {
	this->createInfo = createInfo;
	VK_ERROR_TEST(volkInitialize(), gen::HardwareException, "No Vulkan Support")

	VkApplicationInfo appInfo = { VK_STRUCTURE_TYPE_APPLICATION_INFO, NULL };
	appInfo.pApplicationName = "GongEn Vulkan Implementation";
	appInfo.applicationVersion = VK_MAKE_API_VERSION(0, 1, 0, 0);
	appInfo.pEngineName = "GongEn";
	appInfo.engineVersion = VK_MAKE_API_VERSION(0, 0, 0, 1);
	appInfo.apiVersion = VK_API_VERSION_1_0;

	uint32_t instanceExtensionCount;
	vkEnumerateInstanceExtensionProperties(NULL, &instanceExtensionCount, NULL);
	std::vector<VkExtensionProperties> instanceExtensions(instanceExtensionCount);
	vkEnumerateInstanceExtensionProperties(NULL, &instanceExtensionCount, instanceExtensions.data());

	uint32_t layersCount;;
	vkEnumerateInstanceLayerProperties(&layersCount, NULL);
	std::vector<VkLayerProperties> layers(layersCount);
	vkEnumerateInstanceLayerProperties(&layersCount, layers.data());

	debugCreateInfo = { VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT, NULL };
	debugCreateInfo.flags = 0;
	debugCreateInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
	debugCreateInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
	debugCreateInfo.pfnUserCallback = debugCallback;
	debugCreateInfo.pUserData = nullptr;

	std::vector<const char*> enabledLayers;
	std::vector<const char*> enabledExtensions;

	bool debugUtilsExtension = false;
	for (auto& e : instanceExtensions) {
		if (!std::string(e.extensionName).compare(VK_EXT_DEBUG_UTILS_EXTENSION_NAME)) {
			debugUtilsExtension = true;
			enabledExtensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
		}
		if (!std::string(e.extensionName).compare(VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME)) {
			enabledExtensions.push_back(VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME);
		}
	}
	for (auto& e : layers) {
		if (createInfo.debugMode) {
			if (!std::string(e.layerName).compare("VK_LAYER_KHRONOS_validation")) {
				enabledLayers.push_back("VK_LAYER_KHRONOS_validation");
			}
		}
	}
	
	uint64_t requiredExtensionsCount = 0;
	const char** requiredExtensions = (const char**)windowAPI->graphicsFunction(gen::WindowGraphicsFunction::VK_GET_REQUIRED_EXTENSIONS, 0, 0, 0, requiredExtensionsCount);
	for (uint32_t i = 0; i < requiredExtensionsCount; i++) {
		enabledExtensions.push_back(requiredExtensions[i]);
	}

	VkInstanceCreateInfo instanceCreateInfo = { VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO, &debugCreateInfo };
	instanceCreateInfo.enabledExtensionCount = enabledExtensions.size();
	instanceCreateInfo.enabledLayerCount = enabledLayers.size();
	instanceCreateInfo.flags = 0;
	instanceCreateInfo.pApplicationInfo = &appInfo;
	instanceCreateInfo.ppEnabledExtensionNames = enabledExtensions.data();
	instanceCreateInfo.ppEnabledLayerNames = enabledLayers.data();
	
	VK_ERROR_TEST(vkCreateInstance(&instanceCreateInfo, nullptr, &instance), gen::HardwareException, "Vulkan Instance Creation Failed")

	volkLoadInstance(instance);

	uint32_t devicesCount;
	vkEnumeratePhysicalDevices(instance, &devicesCount, NULL);
	std::vector<VkPhysicalDevice> devices(devicesCount);
	vkEnumeratePhysicalDevices(instance, &devicesCount, devices.data());

	for (uint32_t i = 0; i < devicesCount; i++) {
		VkPhysicalDevice device = devices[i];
		VkPhysicalDeviceProperties deviceProperties;
		vkGetPhysicalDeviceProperties(device, &deviceProperties);
		VkPhysicalDeviceFeatures deviceFeatures;
		vkGetPhysicalDeviceFeatures(device, &deviceFeatures);

		uint32_t queueFamilyCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);
		std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
		vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

		uint32_t graphicsQueue = UINT32_MAX;

		uint64_t temp;
		for (uint32_t i = 0; i < queueFamilyCount; i++) {
			if (queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
				if (windowAPI->graphicsFunction(gen::WindowGraphicsFunction::VK_QUEUE_PRESENTATION_SUPPORT, (uint64_t)instance, (uint64_t)device, (uint64_t)i, temp)) {
					graphicsQueue = i;
				}
			}
		}

		bool swapchainSupport = false;
		uint32_t deviceExtensionCount;
		vkEnumerateDeviceExtensionProperties(device, NULL, &deviceExtensionCount, NULL);
		std::vector<VkExtensionProperties> deviceExtensions(deviceExtensionCount);
		vkEnumerateDeviceExtensionProperties(device, NULL, &deviceExtensionCount, deviceExtensions.data());
		for (auto& e : deviceExtensions) {
			if (!std::string(e.extensionName).compare(VK_KHR_SWAPCHAIN_EXTENSION_NAME)) {
				swapchainSupport = true;
			}
		}
		if (!swapchainSupport) break;

		gen::vkDeviceInfo vkDeviceInfo;
		vkDeviceInfo.device = device;
		vkDeviceInfo.features = deviceFeatures;
		vkDeviceInfo.queueIndex = graphicsQueue;
		if (vkDeviceInfo.queueIndex != UINT32_MAX) {
			gen::GraphicsDeviceInfo deviceInfo;
			deviceInfo.defferedContexts = false;
			deviceInfo.multipleSwapchains = true;
			deviceInfo.name = deviceProperties.deviceName;
			if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_OTHER) deviceInfo.type = gen::GraphicsDeviceType::UNKNOWN;
			else if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU) deviceInfo.type = gen::GraphicsDeviceType::INTEGRATED;
			else if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) deviceInfo.type = gen::GraphicsDeviceType::DISCRETE;
			else if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU) deviceInfo.type = gen::GraphicsDeviceType::VIRTUAL;
			else if (deviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_CPU) deviceInfo.type = gen::GraphicsDeviceType::SOFTWARE;
			
			deviceInfo.features.geometryShader = deviceFeatures.geometryShader;
			deviceInfo.features.tesselationShader = deviceFeatures.tessellationShader;
			deviceInfo.features.computeShader = true;
			deviceInfo.features.multiDrawIndirect = deviceFeatures.multiDrawIndirect;
			deviceInfo.features.drawIndirectFirstInstance = deviceFeatures.drawIndirectFirstInstance;
			deviceInfo.features.firstInstance = true;
			deviceInfo.features.firstVertex = true;
			deviceInfo.features.depthClamp = deviceFeatures.depthClamp;
			deviceInfo.features.independentBlend = deviceFeatures.independentBlend;
			deviceInfo.features.multiViewport = deviceFeatures.multiViewport;
			deviceInfo.features.vertexPipelineStoresAndAtomics = deviceFeatures.vertexPipelineStoresAndAtomics;
			deviceInfo.features.fragmentStoresAndAtomics = deviceFeatures.fragmentStoresAndAtomics;
			deviceInfo.features.samplerAnisotropy = deviceFeatures.samplerAnisotropy;
			deviceInfo.features.imageCubeArray = deviceFeatures.imageCubeArray;
			deviceInfo.features.textureCompressionETC2 = deviceFeatures.textureCompressionETC2;
			deviceInfo.features.textureCompressionASTC_LDR = deviceFeatures.textureCompressionASTC_LDR;
			deviceInfo.features.textureCompressionBC1_3 = deviceFeatures.textureCompressionBC;
			deviceInfo.features.textureCompressionBC4_5 = deviceFeatures.textureCompressionBC;
			deviceInfo.features.textureCompressionBC6_7 = deviceFeatures.textureCompressionBC;
			deviceInfo.features.shaderFloat64 = deviceFeatures.shaderFloat64;
			deviceInfo.features.shaderInt64 = deviceFeatures.shaderInt64;
			deviceInfo.features.shaderInt16 = deviceFeatures.shaderInt16;

			deviceInfo.limits.maxTextureDimension1D = deviceProperties.limits.maxImageDimension1D;
			deviceInfo.limits.maxTextureDimension2D = deviceProperties.limits.maxImageDimension2D;
			deviceInfo.limits.maxTextureDimension3D = deviceProperties.limits.maxImageDimension3D;
			deviceInfo.limits.maxTextureDimensionCube = deviceProperties.limits.maxImageDimensionCube;
			deviceInfo.limits.maxTextureArrayLayers = deviceProperties.limits.maxImageArrayLayers;
			deviceInfo.limits.maxUniformBufferRange = deviceProperties.limits.maxUniformBufferRange;
			deviceInfo.limits.maxStorageBufferRange = deviceProperties.limits.maxStorageBufferRange;
			deviceInfo.limits.maxColorAttachments = deviceProperties.limits.maxColorAttachments;
			deviceInfo.limits.maxComputeWorkGroupCount[0] = deviceProperties.limits.maxComputeWorkGroupCount[0];
			deviceInfo.limits.maxComputeWorkGroupCount[1] = deviceProperties.limits.maxComputeWorkGroupCount[1];
			deviceInfo.limits.maxComputeWorkGroupCount[2] = deviceProperties.limits.maxComputeWorkGroupCount[2];
			deviceInfo.limits.maxDrawIndirectCount = deviceProperties.limits.maxDrawIndirectCount;
			deviceInfo.limits.maxViewports = deviceProperties.limits.maxViewports;

			deviceInfo.reserved = i;

			//Get Amount of VRAM
			deviceInfo.vram = 0;
			VkPhysicalDeviceMemoryProperties memoryProperties;
			vkGetPhysicalDeviceMemoryProperties(device, &memoryProperties);
			for (uint32_t h = 0; h < memoryProperties.memoryHeapCount; h++) {
				if (memoryProperties.memoryHeaps[h].flags & VK_MEMORY_HEAP_DEVICE_LOCAL_BIT) {
					deviceInfo.vram = memoryProperties.memoryHeaps[h].size;
					break;
				}
			}

			this->devices.push_back(deviceInfo);
			vkDevices.push_back(vkDeviceInfo);
		}
	}
}

gen::GraphicsDevice* gen::vkGraphicsAPI::createDevice(GraphicsDeviceInfo* deviceInfo, Window* window, Swapchain** swaphain) {
	if (devices.empty()) init(window->getWindowAPI(), createInfo);
	if (devices.empty()) {
		throw gen::NoSuitableDeviceException("No Suitable Vulkan Device");
	}
	GraphicsDeviceInfo* graphicsDevice = deviceInfo;
	if (!graphicsDevice) {
		for (uint32_t i = 0; i < devices.size(); i++) {
			graphicsDevice = &devices[i];
			break;
		}
	}
	vkDeviceInfo* vkGraphicsDevice = nullptr;
	if (!graphicsDevice) return nullptr;
	else vkGraphicsDevice = &vkDevices[graphicsDevice->reserved];
	
	VkDeviceQueueCreateInfo queueCreateInfo{ VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO, NULL };
	queueCreateInfo.flags = 0;
	queueCreateInfo.queueFamilyIndex = vkGraphicsDevice->queueIndex;
	queueCreateInfo.queueCount = 1;
	float queuePriority = 1.0f;
	queueCreateInfo.pQueuePriorities = &queuePriority;

	uint32_t deviceExtensionCount;
	vkEnumerateDeviceExtensionProperties(vkGraphicsDevice->device, NULL, &deviceExtensionCount, NULL);
	std::vector<VkExtensionProperties> deviceExtensions(deviceExtensionCount);
	vkEnumerateDeviceExtensionProperties(vkGraphicsDevice->device, NULL, &deviceExtensionCount, deviceExtensions.data());

	uint32_t layersCount;
	vkEnumerateDeviceLayerProperties(vkGraphicsDevice->device, &layersCount, NULL);
	std::vector<VkLayerProperties> layers(layersCount);
	vkEnumerateDeviceLayerProperties(vkGraphicsDevice->device, &layersCount, layers.data());

	std::vector<const char*> enabledExtensions;
	std::vector<const char*> enabledLayers;
	
	bool VK_KHR_DEDICATED_ALLOCATION = false;
	bool VK_KHR_GET_MEMORY_REQUIREMENTS_2 = false;
	bool VK_EXT_MEMORY_BUDGET = false;
	bool VK_KHR_MAINTENANCE1 = false;

	enabledExtensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
	for (auto& e : deviceExtensions) {
		if (!std::string(e.extensionName).compare(VK_KHR_DEDICATED_ALLOCATION_EXTENSION_NAME)) {
			enabledExtensions.push_back(VK_KHR_DEDICATED_ALLOCATION_EXTENSION_NAME);
			VK_KHR_DEDICATED_ALLOCATION = true;
		}
		if (!std::string(e.extensionName).compare(VK_KHR_GET_MEMORY_REQUIREMENTS_2_EXTENSION_NAME)) {
			enabledExtensions.push_back(VK_KHR_GET_MEMORY_REQUIREMENTS_2_EXTENSION_NAME);
			VK_KHR_GET_MEMORY_REQUIREMENTS_2 = true;
		}
		if (!std::string(e.extensionName).compare(VK_EXT_MEMORY_BUDGET_EXTENSION_NAME)) {
			enabledExtensions.push_back(VK_EXT_MEMORY_BUDGET_EXTENSION_NAME);
			VK_EXT_MEMORY_BUDGET = true;
		}
		if (!std::string(e.extensionName).compare(VK_KHR_MAINTENANCE1_EXTENSION_NAME)) {
			enabledExtensions.push_back(VK_KHR_MAINTENANCE1_EXTENSION_NAME);
			VK_KHR_MAINTENANCE1 = true;
		}
	}

	if (!VK_KHR_MAINTENANCE1) {
		return nullptr;
	}

	for (auto& e : layers) {
		if (createInfo.debugMode) {
			if (!std::string(e.layerName).compare("VK_LAYER_KHRONOS_validation")) {
				enabledLayers.push_back("VK_LAYER_KHRONOS_validation");
			}
		}
	}

	VkDeviceCreateInfo deviceCreateInfo{ VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO, NULL };
	deviceCreateInfo.flags = 0;
	deviceCreateInfo.queueCreateInfoCount = 1;
	deviceCreateInfo.pQueueCreateInfos = &queueCreateInfo;
	deviceCreateInfo.pEnabledFeatures = &vkGraphicsDevice->features;
	deviceCreateInfo.ppEnabledExtensionNames = enabledExtensions.data();
	deviceCreateInfo.ppEnabledLayerNames = enabledLayers.data();
	deviceCreateInfo.enabledExtensionCount = enabledExtensions.size();
	deviceCreateInfo.enabledLayerCount = enabledLayers.size();

	VkDevice device;
	VK_ERROR_TEST(vkCreateDevice(vkGraphicsDevice->device, &deviceCreateInfo, nullptr, &device), gen::HardwareException, "Vulkan Device Creation Failed")

	volkLoadDevice(device);

	VkQueue queue;
	vkGetDeviceQueue(device, vkGraphicsDevice->queueIndex, 0, &queue);

	VmaVulkanFunctions vmaFunctions;
	memset(&vmaFunctions, 0, sizeof(VmaVulkanFunctions));
	vmaFunctions.vkGetInstanceProcAddr = vkGetInstanceProcAddr;
	vmaFunctions.vkGetDeviceProcAddr = vkGetDeviceProcAddr;
	vmaFunctions.vkGetPhysicalDeviceMemoryProperties = vkGetPhysicalDeviceMemoryProperties;
	vmaFunctions.vkGetPhysicalDeviceProperties = vkGetPhysicalDeviceProperties;
	vmaFunctions.vkAllocateMemory = vkAllocateMemory;
	vmaFunctions.vkFreeMemory = vkFreeMemory;
	vmaFunctions.vkMapMemory = vkMapMemory;
	vmaFunctions.vkUnmapMemory = vkUnmapMemory;
	vmaFunctions.vkFlushMappedMemoryRanges = vkFlushMappedMemoryRanges;
	vmaFunctions.vkInvalidateMappedMemoryRanges = vkInvalidateMappedMemoryRanges;
	vmaFunctions.vkBindBufferMemory = vkBindBufferMemory;
	vmaFunctions.vkBindImageMemory = vkBindImageMemory;
	vmaFunctions.vkGetBufferMemoryRequirements = vkGetBufferMemoryRequirements;
	vmaFunctions.vkGetImageMemoryRequirements = vkGetImageMemoryRequirements;
	vmaFunctions.vkCreateBuffer = vkCreateBuffer;
	vmaFunctions.vkDestroyBuffer = vkDestroyBuffer;
	vmaFunctions.vkCreateImage = vkCreateImage;
	vmaFunctions.vkDestroyImage = vkDestroyImage;
	vmaFunctions.vkCmdCopyBuffer = vkCmdCopyBuffer;

	uint32_t vmaFlags = 0;
	//if (VK_KHR_DEDICATED_ALLOCATION && VK_KHR_GET_MEMORY_REQUIREMENTS_2) vmaFlags |= VMA_ALLOCATOR_CREATE_KHR_DEDICATED_ALLOCATION_BIT;
	//if (VK_EXT_MEMORY_BUDGET) vmaFlags |= VMA_ALLOCATOR_CREATE_EXT_MEMORY_BUDGET_BIT;

	VmaAllocatorCreateInfo vmaCreateInfo;
	vmaCreateInfo.instance = instance;
	vmaCreateInfo.physicalDevice = vkGraphicsDevice->device;
	vmaCreateInfo.device = device;
	vmaCreateInfo.preferredLargeHeapBlockSize = 0;
	vmaCreateInfo.pAllocationCallbacks = nullptr;
	vmaCreateInfo.pHeapSizeLimit = nullptr;
	vmaCreateInfo.pVulkanFunctions = &vmaFunctions;
	vmaCreateInfo.vulkanApiVersion = VK_API_VERSION_1_0;
	vmaCreateInfo.pTypeExternalMemoryHandleTypes = nullptr;
	vmaCreateInfo.pDeviceMemoryCallbacks = nullptr;
	vmaCreateInfo.flags = vmaFlags;

	VmaAllocator allocator;
	VK_ERROR_TEST(vmaCreateAllocator(&vmaCreateInfo, &allocator), gen::HardwareException, "Vulkan Memory Allocator Creation Failed")

	gen::vkGraphicsDevice* out = new gen::vkGraphicsDevice(device, allocator, vkGraphicsDevice->queueIndex, queue, instance, vkGraphicsDevice->device, createInfo);
	*swaphain = new gen::vkSwapchain(window, out);
	return out;
}

VkFormat formatConversionTable[(size_t)gen::GPUFormat::TOTAL_TEXTURE_FORMATS];
void setupFormatConversionTable();

gen::vkGraphicsDevice::vkGraphicsDevice(VkDevice device, VmaAllocator allocator, uint32_t queueFamily, VkQueue queue, VkInstance instance, VkPhysicalDevice physicalDevice, gen::GraphicsAPICreateInfo createInfo) {
	this->device = device;
	this->allocator = allocator;
	this->queueFamily = queueFamily;
	this->queue = queue;
	this->instance = instance;
	this->physicalDevice = physicalDevice;
	this->createInfo = createInfo;
	setupFormatConversionTable();
	
	poolCreateInfo.blockSize = 0;
	poolCreateInfo.minBlockCount = 0;
	poolCreateInfo.maxBlockCount = 0;
	poolCreateInfo.priority = 1.0f;
	poolCreateInfo.minAllocationAlignment = 0;
	poolCreateInfo.pMemoryAllocateNext = nullptr;
	poolCreateInfo.flags = VMA_POOL_CREATE_LINEAR_ALGORITHM_BIT;
	
	VkPhysicalDeviceMemoryProperties memoryProperties;
	vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memoryProperties);
	for (uint32_t i = 0; i < memoryProperties.memoryTypeCount; i++) {
		uint32_t memFlags = memoryProperties.memoryTypes[i].propertyFlags;
		if ((memFlags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT) && (memFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)) {
			if (memFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) {
				memoryType_CPUToGPU = i;
			}
			else {
				memoryType_CPU = i;
			}
		}
	}

	VkPipelineCacheCreateInfo cacheCreateInfo = { VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO, NULL };
	cacheCreateInfo.flags = 0;
	cacheCreateInfo.initialDataSize = 0;
	cacheCreateInfo.pInitialData = nullptr;

	VK_ERROR_TEST(vkCreatePipelineCache(device, &cacheCreateInfo, nullptr, &pipelineCache), gen::GraphicsRuntimeException, "Vulkan Pipeline Cache Creation Failed");

	VkCommandPoolCreateInfo commandPoolCreateInfo = { VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO, NULL };
	commandPoolCreateInfo.queueFamilyIndex = this->queueFamily;
	commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT | VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
	VK_ERROR_TEST(vkCreateCommandPool(device, &commandPoolCreateInfo, nullptr, &commandPool), gen::GraphicsRuntimeException, "Vulkan Command Pool Creation Failed")
}

void setupFormatConversionTable() {
	formatConversionTable[(size_t)gen::GPUFormat::BC1_SRGB] = VK_FORMAT_BC1_RGB_SRGB_BLOCK;
	formatConversionTable[(size_t)gen::GPUFormat::BC1_UNORM] = VK_FORMAT_BC1_RGB_UNORM_BLOCK;
	formatConversionTable[(size_t)gen::GPUFormat::BC3_SRGB] = VK_FORMAT_BC3_SRGB_BLOCK;
	formatConversionTable[(size_t)gen::GPUFormat::BC3_UNORM] = VK_FORMAT_BC3_UNORM_BLOCK;
	formatConversionTable[(size_t)gen::GPUFormat::BC4_UNORM] = VK_FORMAT_BC4_UNORM_BLOCK;
	formatConversionTable[(size_t)gen::GPUFormat::BC4_SNORM] = VK_FORMAT_BC4_SNORM_BLOCK;
	formatConversionTable[(size_t)gen::GPUFormat::BC5_UNORM] = VK_FORMAT_BC5_UNORM_BLOCK;
	formatConversionTable[(size_t)gen::GPUFormat::BC5_SNORM] = VK_FORMAT_BC5_SNORM_BLOCK;
	formatConversionTable[(size_t)gen::GPUFormat::BC6H_SFLOAT] = VK_FORMAT_BC6H_SFLOAT_BLOCK;
	formatConversionTable[(size_t)gen::GPUFormat::BC6H_UFLOAT] = VK_FORMAT_BC6H_UFLOAT_BLOCK;
	formatConversionTable[(size_t)gen::GPUFormat::BC7_SRGB] = VK_FORMAT_BC7_SRGB_BLOCK;
	formatConversionTable[(size_t)gen::GPUFormat::BC7_UNORM] = VK_FORMAT_BC7_UNORM_BLOCK;

	formatConversionTable[(size_t)gen::GPUFormat::ETC2_RGBA_SRGB] = VK_FORMAT_ETC2_R8G8B8A8_SRGB_BLOCK;
	formatConversionTable[(size_t)gen::GPUFormat::ETC2_RGBA_UNORM] = VK_FORMAT_ETC2_R8G8B8A8_UNORM_BLOCK;
	formatConversionTable[(size_t)gen::GPUFormat::ETC2_RGB_SRGB] = VK_FORMAT_ETC2_R8G8B8_SRGB_BLOCK;
	formatConversionTable[(size_t)gen::GPUFormat::ETC2_RGB_UNORM] = VK_FORMAT_ETC2_R8G8B8_UNORM_BLOCK;
	formatConversionTable[(size_t)gen::GPUFormat::EAC_RG_SNORM] = VK_FORMAT_EAC_R11G11_SNORM_BLOCK;
	formatConversionTable[(size_t)gen::GPUFormat::EAC_RG_UNORM] = VK_FORMAT_EAC_R11G11_UNORM_BLOCK;
	formatConversionTable[(size_t)gen::GPUFormat::EAC_R_SNORM] = VK_FORMAT_EAC_R11_SNORM_BLOCK;
	formatConversionTable[(size_t)gen::GPUFormat::EAC_R_UNORM] = VK_FORMAT_EAC_R11_UNORM_BLOCK;

	formatConversionTable[(size_t)gen::GPUFormat::R8_SINT] = VK_FORMAT_R8_SINT;
	formatConversionTable[(size_t)gen::GPUFormat::R8_SNORM] = VK_FORMAT_R8_SNORM;
	formatConversionTable[(size_t)gen::GPUFormat::R8_UINT] = VK_FORMAT_R8_UINT;
	formatConversionTable[(size_t)gen::GPUFormat::R8_UNORM] = VK_FORMAT_R8_UNORM;
	formatConversionTable[(size_t)gen::GPUFormat::R16_SFLOAT] = VK_FORMAT_R16_SFLOAT;
	formatConversionTable[(size_t)gen::GPUFormat::R16_SINT] = VK_FORMAT_R16_SINT;
	formatConversionTable[(size_t)gen::GPUFormat::R16_SNORM] = VK_FORMAT_R16_SNORM;
	formatConversionTable[(size_t)gen::GPUFormat::R16_UINT] = VK_FORMAT_R16_UINT;
	formatConversionTable[(size_t)gen::GPUFormat::R16_UNORM] = VK_FORMAT_R16_UNORM;
	formatConversionTable[(size_t)gen::GPUFormat::R32_SFLOAT] = VK_FORMAT_R32_SFLOAT;
	formatConversionTable[(size_t)gen::GPUFormat::R32_SINT] = VK_FORMAT_R32_SINT;
	formatConversionTable[(size_t)gen::GPUFormat::R32_UINT] = VK_FORMAT_R32_UINT;

	formatConversionTable[(size_t)gen::GPUFormat::RG8_SINT] = VK_FORMAT_R8G8_SINT;
	formatConversionTable[(size_t)gen::GPUFormat::RG8_SNORM] = VK_FORMAT_R8G8_SNORM;
	formatConversionTable[(size_t)gen::GPUFormat::RG8_UINT] = VK_FORMAT_R8G8_UINT;
	formatConversionTable[(size_t)gen::GPUFormat::RG8_UNORM] = VK_FORMAT_R8G8_UNORM;
	formatConversionTable[(size_t)gen::GPUFormat::RG16_SFLOAT] = VK_FORMAT_R16G16_SFLOAT;
	formatConversionTable[(size_t)gen::GPUFormat::RG16_SINT] = VK_FORMAT_R16G16_SINT;
	formatConversionTable[(size_t)gen::GPUFormat::RG16_SNORM] = VK_FORMAT_R16G16_SNORM;
	formatConversionTable[(size_t)gen::GPUFormat::RG16_UINT] = VK_FORMAT_R16G16_UINT;
	formatConversionTable[(size_t)gen::GPUFormat::RG16_UNORM] = VK_FORMAT_R16G16_UNORM;
	formatConversionTable[(size_t)gen::GPUFormat::RG32_SFLOAT] = VK_FORMAT_R32G32_SFLOAT;
	formatConversionTable[(size_t)gen::GPUFormat::RG32_SINT] = VK_FORMAT_R32G32_SINT;
	formatConversionTable[(size_t)gen::GPUFormat::RG32_UINT] = VK_FORMAT_R32G32_UINT;

	formatConversionTable[(size_t)gen::GPUFormat::RGBA8_SINT] = VK_FORMAT_R8G8B8A8_SINT;
	formatConversionTable[(size_t)gen::GPUFormat::RGBA8_SNORM] = VK_FORMAT_R8G8B8A8_SNORM;
	formatConversionTable[(size_t)gen::GPUFormat::RGBA8_UINT] = VK_FORMAT_R8G8B8A8_UINT;
	formatConversionTable[(size_t)gen::GPUFormat::RGBA8_UNORM] = VK_FORMAT_R8G8B8A8_UNORM;
	formatConversionTable[(size_t)gen::GPUFormat::RGBA8_UNORM_SRGB] = VK_FORMAT_R8G8B8A8_SRGB;
	formatConversionTable[(size_t)gen::GPUFormat::RGBA16_SFLOAT] = VK_FORMAT_R16G16B16A16_SFLOAT;
	formatConversionTable[(size_t)gen::GPUFormat::RGBA16_SINT] = VK_FORMAT_R16G16B16A16_SINT;
	formatConversionTable[(size_t)gen::GPUFormat::RGBA16_SNORM] = VK_FORMAT_R16G16B16A16_SNORM;
	formatConversionTable[(size_t)gen::GPUFormat::RGBA16_UINT] = VK_FORMAT_R16G16B16A16_UINT;
	formatConversionTable[(size_t)gen::GPUFormat::RGBA16_UNORM] = VK_FORMAT_R16G16B16A16_UNORM;
	formatConversionTable[(size_t)gen::GPUFormat::RGBA32_SFLOAT] = VK_FORMAT_R32G32B32A32_SFLOAT;
	formatConversionTable[(size_t)gen::GPUFormat::RGBA32_SINT] = VK_FORMAT_R32G32B32A32_SINT;
	formatConversionTable[(size_t)gen::GPUFormat::RGBA32_UINT] = VK_FORMAT_R32G32B32A32_UINT;

	formatConversionTable[(size_t)gen::GPUFormat::D16_UNORM] = VK_FORMAT_D16_UNORM;
	formatConversionTable[(size_t)gen::GPUFormat::D24_UNORM_S8_UINT] = VK_FORMAT_D24_UNORM_S8_UINT;
	formatConversionTable[(size_t)gen::GPUFormat::D32_SFLOAT] = VK_FORMAT_D32_SFLOAT;

	formatConversionTable[(size_t)gen::GPUFormat::BGRA8_SRGB] = VK_FORMAT_B8G8R8A8_SRGB;
	formatConversionTable[(size_t)gen::GPUFormat::BGRA8_UNORM] = VK_FORMAT_B8G8R8A8_UNORM;

	formatConversionTable[(size_t)gen::GPUFormat::RGB8_SINT] = VK_FORMAT_R8G8B8_SINT;
	formatConversionTable[(size_t)gen::GPUFormat::RGB8_SNORM] = VK_FORMAT_R8G8B8_SNORM;
	formatConversionTable[(size_t)gen::GPUFormat::RGB8_UINT] = VK_FORMAT_R8G8B8_UINT;
	formatConversionTable[(size_t)gen::GPUFormat::RGB8_UNORM] = VK_FORMAT_R8G8B8_UNORM;
	formatConversionTable[(size_t)gen::GPUFormat::RGB8_UNORM_SRGB] = VK_FORMAT_R8G8B8_SRGB;
	formatConversionTable[(size_t)gen::GPUFormat::RGB16_SFLOAT] = VK_FORMAT_R16G16B16_SFLOAT;
	formatConversionTable[(size_t)gen::GPUFormat::RGB16_SINT] = VK_FORMAT_R16G16B16_SINT;
	formatConversionTable[(size_t)gen::GPUFormat::RGB16_SNORM] = VK_FORMAT_R16G16B16_SNORM;
	formatConversionTable[(size_t)gen::GPUFormat::RGB16_UINT] = VK_FORMAT_R16G16B16_UINT;
	formatConversionTable[(size_t)gen::GPUFormat::RGB16_UNORM] = VK_FORMAT_R16G16B16_UNORM;
	formatConversionTable[(size_t)gen::GPUFormat::RGB32_SFLOAT] = VK_FORMAT_R32G32B32_SFLOAT;
	formatConversionTable[(size_t)gen::GPUFormat::RGB32_SINT] = VK_FORMAT_R32G32B32_SINT;
	formatConversionTable[(size_t)gen::GPUFormat::RGB32_UINT] = VK_FORMAT_R32G32B32_UINT;
}
