#include "vfs.hpp"
#include <gongen/gcf/gcf.hpp>

/*gen::VFS* gen::createVFS() {
	return new gen::VFSImpl();
}

void gen::VFSImpl::mount(const std::string& realPath, const std::string& vfsPath) {
	if (realPath.empty()) {
		mountDir("", vfsPath);
		return;
	}
	bool directory = gen::isDir(realPath.c_str());
	if (directory) {
		//Physical Mount
		mountDir(realPath, vfsPath);
	}
	else {
		//GCF Mount
		gen::GCFHandle *handle = new gen::GCFHandle();
		if (gen::openGCF(*handle, realPath)) {
			mount(handle, vfsPath);
			loadedGCFFiles.push_back(handle);
		}
		else {
			gen::freeGCF(*handle);
			delete handle;
		}
	}
}

void gen::VFSImpl::mountDir(std::string dir, std::string vfsPath) {
	std::vector<gen::enumaratedFile> enumaratedFiles;
	if(dir.empty()) enumaratedFiles = gen::enumarateDir(NULL);
	else enumaratedFiles = gen::enumarateDir(dir.c_str());
	for (const auto& entry : enumaratedFiles) {
		std::string name = entry.name;

		std::string physicalPath = (dir + "/" + name);
		if (dir.empty()) physicalPath = name;

		gen::VFSNode node;
		node.name = name;
		node.type = gen::VFSNodeType::PHYSICAL;
		node.physicalPath = physicalPath;
		node.gcfID = gen::INVALID_VFS_NODE;
		node.directory = entry.isDirectory;

		std::string vfsNodePath = (vfsPath + "/" + name);
		if (vfsPath.empty()) vfsNodePath = name;

		nodes[vfsNodePath] = node;

		if (entry.isDirectory) {
			mountDir(physicalPath, vfsNodePath);
		}
	}
}

bool vfs_stringCompare(const char* c1, const char* c2) {
	uint32_t l1 = strlen(c1);
	uint32_t l2 = strlen(c2);

	if (l1 != l2) return false;
	else {
		for (uint32_t i = 0; i < l1; i++) {
			if (c1[i] != c2[i]) return false;
		}
	}
	return true;
}

std::string vfs_str(const char* cstr) {
	if (cstr) {
		return std::string(cstr);
	}
	else return std::string();
}

void gen::VFSImpl::mountGCF(tinyxml2::XMLElement* pNode, std::string& vfsPath, uint32_t gcfID) {
	std::string vfsPath2 = vfsPath;

	tinyxml2::XMLElement* pSubNode = pNode->FirstChildElement();

	while (pSubNode) {
		if (vfs_stringCompare(pSubNode->Name(), "Node")) {
			std::string type = vfs_str(pSubNode->Attribute("type"));
			std::string name = vfs_str(pSubNode->Attribute("name"));
			bool mountNode = false;
			bool dir = false;

			gen::VFSNode node;
			node.name = name;
			node.type = gen::VFSNodeType::GCF;
			node.gcfOffset = pSubNode->Unsigned64Attribute("dataOffset");
			node.gcfCompressedSize = pSubNode->Unsigned64Attribute("dataSize");
			node.gcfUncompressedSize = pSubNode->Unsigned64Attribute("uncompressedSize");
			node.gcfCompression = pSubNode->UnsignedAttribute("dataCompression");
			node.gcfID = gcfID;

			if (vfs_stringCompare(type.c_str(), "file")) {
				mountNode = true;
			}
			else if (vfs_stringCompare(type.c_str(), "dir")) {
				dir = true;
				mountNode = true;
			}
			node.directory = dir;

			if (mountNode) {
				std::string vfsNodePath = (vfsPath + "/" + name);
				if (vfsPath.empty()) vfsNodePath = name;

				nodes[vfsNodePath] = node;

				if (dir) {
					vfsPath2 = vfsNodePath;
					mountGCF(pSubNode, vfsPath2, gcfID);
				}
			}
		}

		pSubNode = pSubNode->NextSiblingElement();
	}
}

void gen::VFSImpl::mount(gen::GCFHandle* handle, const std::string& vfsPath) {
	std::string vfsPath2 = vfsPath;

	uint32_t id = loadedGCFFiles.size();
	tinyxml2::XMLElement* pRoot = handle->xmlDoc.FirstChildElement("GCF");
	if (pRoot) {
		mountGCF(pRoot, vfsPath2, id);
	}
}

void gen::VFSImpl::unmountAll() {
	nodes.clear();
	for (auto g : loadedGCFFiles) {
		gen::freeGCF(*g);
		delete g;
	}
	loadedGCFFiles.clear();
}

gen::VFSHandlePtr gen::VFSImpl::open(const std::string& path, gen::FileOpenMode mode) {
	gen::VFSHandle* handle = new gen::VFSHandle();
	handle->type = gen::VFSNodeType::PHYSICAL;
	handle->open = false;

	if (nodes.find(path) != nodes.end()) {
		handle->open = true;

		gen::VFSNode* node = &nodes[path];
		handle->type = node->type;
		if (handle->type == gen::VFSNodeType::PHYSICAL) {
			handle->fhandle = gen::openFile(node->physicalPath.c_str(), mode);
		}
		else if (handle->type == gen::VFSNodeType::GCF) {
			handle->fhandle.mode = gen::FileOpenMode::READ;
			handle->gcfOffset = node->gcfOffset;
			handle->gcfCompressedSize = node->gcfCompressedSize;
			handle->gcfUncompressedSize = node->gcfUncompressedSize;
			handle->gcfCompression = node->gcfCompression;
			if (node->gcfID != gen::INVALID_VFS_NODE) {
				handle->gcf = loadedGCFFiles[node->gcfID];
			}
			else {
				handle->gcf = nullptr;
			}
		}
	}
	
	return gen::VFSHandlePtr(handle);
}

void* gen::VFSImpl::map(VFSHandlePtr& handle, gen::MapMode mode) {
	gen::VFSHandle* handle2 = handle.get();
	if (handle2->type == gen::VFSNodeType::PHYSICAL) {
		if (!handle2->fhandle.mapped_addr) {
			gen::mapFile(handle2->fhandle, mode, nullptr, 0, 0);
		}
		return handle2->fhandle.mapped_addr;
	}
	else if (handle2->type == gen::VFSNodeType::GCF) {
		if ((uint32_t)handle2->gcfCompression) return nullptr;
		else return &handle2->gcf->data[handle2->gcfOffset];
	}
	return nullptr;
}

uint64_t gen::VFSImpl::fileSize(VFSHandlePtr& handle) {
	gen::VFSHandle* handle2 = handle.get();
	if (handle2->type == gen::VFSNodeType::PHYSICAL) {
		return gen::fileSize(handle2->fhandle);
	}
	else if (handle2->type == gen::VFSNodeType::GCF) {
		return handle2->gcfUncompressedSize;
	}
	return 0;
}

uint64_t gen::VFSImpl::read(VFSHandlePtr& handle, uint64_t offset, uint64_t size, void* data) {
	gen::VFSHandle* handle2 = handle.get();
	if (handle2->type == gen::VFSNodeType::PHYSICAL) {
		gen::fileSeek(handle->fhandle, offset, gen::SeekType::SET);
		return gen::fileRead(handle->fhandle, size, data);
	}
	else if (handle2->type == gen::VFSNodeType::GCF) {
		gen::gcfGetData(*handle->gcf, data, offset, handle2->gcfCompressedSize, size, (gen::CompressionMode)handle2->gcfCompression);
		return size;
	}
	return 0;
}

uint64_t gen::VFSImpl::write(VFSHandlePtr& handle, uint64_t offset, uint64_t size, void* data) {
	gen::VFSHandle* handle2 = handle.get();
	if (handle2->type == gen::VFSNodeType::PHYSICAL) {
		gen::fileSeek(handle->fhandle, offset, gen::SeekType::SET);
		return gen::fileWrite(handle->fhandle, size, data);
	}
	else if (handle2->type == gen::VFSNodeType::GCF) {
		return 0;
	}
	return 0;
}

bool gen::VFSImpl::isDir(const std::string& path) {
	if (nodes.find(path) != nodes.end()) {
		return nodes[path].directory;
	}
	else return false;
}

#include <filesystem>

std::vector<gen::enumaratedFile> gen::VFSImpl::enumarate(const std::string& path) {
	std::vector<std::pair<std::string, std::string>> nodesToCheck;

	for (auto& n : nodes) {
		uint32_t strlen = std::min<uint32_t>(path.size(), n.first.size());

		if (!memcmp(n.first.c_str(), path.c_str(), strlen)) {
			bool valid = false;
			
			if (path.size() != n.first.size()) {
				std::filesystem::path p(n.first.substr(strlen + 1));
				if (strlen == 0) p = std::filesystem::path(n.first);

				if (!valid) {
					if ((p.has_filename()) && (!p.has_parent_path())) {
						valid = true;
					}
				}

				if (valid) nodesToCheck.push_back({ p.string(), n.first });
			}
		}
	}

	std::vector<gen::enumaratedFile> out;

	for (auto& n : nodesToCheck) {
		gen::enumaratedFile f;
		f.name = n.first;
		f.isDirectory = isDir(n.second);
		out.push_back(f);
	}

	return out;
}

void* gen::VFSImpl::map(VFSHandlePtr& handle) {
	if (isMappable(handle)) {
		if (handle->type == gen::VFSNodeType::PHYSICAL) {
			return gen::mapFile(handle->fhandle, gen::MapMode::READ, nullptr, 0, 0);
		}
		else if (handle->type == gen::VFSNodeType::GCF) {
			return &handle->gcf->data[handle->gcfOffset];
		}
	}
	return nullptr;
}

bool gen::VFSImpl::isCompressed(VFSHandlePtr& handle) {
	if (handle->type == gen::VFSNodeType::PHYSICAL) return false;
	else {
		return (handle->gcfCompression != 0);
	}
}

bool gen::VFSImpl::isMappable(VFSHandlePtr& handle) {
	return isCompressed(handle);
}*/
