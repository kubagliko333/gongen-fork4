#pragma once

#include <gongen/vfs/vfs.hpp>

/*namespace gen {
	class GEN_EXPORTED VFSImpl :public gen::VFS {
		std::unordered_map<std::string, VFSNode> nodes;
		std::vector<gen::GCFHandle*> loadedGCFFiles;

		void mountGCF(tinyxml2::XMLElement* pNode, std::string& vfsPath, uint32_t gcfID);
		void mountDir(std::string dir, std::string vfsPath);
	public:
		void mount(const std::string& realPath, const std::string& vfsPath);
		void mount(gen::GCFHandle* handle, const std::string& vfsPath);
		void unmountAll();

		VFSHandlePtr open(const std::string& path, gen::FileOpenMode mode);
		void* map(VFSHandlePtr& handle, gen::MapMode mode);
		uint64_t fileSize(VFSHandlePtr& handle);
		uint64_t read(VFSHandlePtr& handle, uint64_t offset, uint64_t size, void* data);
		uint64_t write(VFSHandlePtr& handle, uint64_t offset, uint64_t size, void* data);

		bool isDir(const std::string& path);
		std::vector<gen::enumaratedFile> enumarate(const std::string& path);

		void* map(VFSHandlePtr& handle);

		bool isCompressed(VFSHandlePtr& handle);
		bool isMappable(VFSHandlePtr& handle);
	};
}*/
