#pragma once

#include <gongen/window_api/window.hpp>
#include "../graphics_vulkan_impl/vulkan.hpp"
#include <GLFW/glfw3.h>
#include <unordered_map>

namespace gen {
	struct GLFWWindowUserData {
		uint32_t windowID;
		WindowAPI* api;
		Window* ptr;
	};

	class GLFWWindow :public Window {
		GLFWwindow* window;
		GLFWWindowUserData userData;
		gen::WindowAPI* api;

		std::unordered_map<uint32_t, GLFWcursor*> cursors;
	public:
		GLFWWindow(GLFWwindow *window, GLFWWindowUserData userData, gen::WindowAPI *api);
		~GLFWWindow();
		bool isOpen();
		void close();
		void setTitle(const std::string& title);
		void resize(uint16_t width, uint16_t height);
		void setClipboard(const std::string& content);
		std::string getClipboard();
		uint64_t graphicsFunction(WindowGraphicsFunction func, uint64_t userData, uint64_t userData2, uint64_t userData3, uint64_t& out2);
		gen::WindowAPI* getWindowAPI() { return api; }
		uint32_t getWidth();
		uint32_t getHeight();
		void setCursor(gen::Cursor cursor);
		void setMousePosition(uint32_t x, uint32_t y);
	};

	class GLFWWindowAPI :public WindowAPI {
	public:
		GLFWWindowAPI();
		~GLFWWindowAPI();
		Window* createWindow(const WindowCreateInfo& info);
		void poolEvents();
		void waitEvents();
		uint64_t graphicsFunction(WindowGraphicsFunction func, uint64_t userData, uint64_t userData2, uint64_t userData3, uint64_t &out2);
	};
}
