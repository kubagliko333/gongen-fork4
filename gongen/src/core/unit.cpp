#include <gongen/core/unit.hpp>
#include <gongen/core/core.hpp>

gen::Unit::Unit(Core* core, UnitMeta* meta, size_t instanceIndex) : Unit(core, meta, instanceIndex,
		core->addLogger((new gen::LoggerBuilder(core, gen::identifier("gen", "unit_" + meta->identifier.namespaceId + "_" + meta->identifier.itemId + "_" + std::to_string(instanceIndex))))->setName(meta->name))) {
}

gen::Unit::Unit(Core* core, UnitMeta* meta, size_t instanceIndex, Logger* logger) : core(core), meta(meta), instanceIndex(instanceIndex), logger(logger) {
}

const gen::BasicRegistrySettings unitRegistryBasicRegistrySettings = gen::BasicRegistrySettings{.allowRemoving = false, .allowClosing = true};
gen::UnitRegistry::UnitRegistry(Core* core, std::string id) : gen::Registry<RegisteredForeign<UnitMeta>>(unitRegistryBasicRegistrySettings), gen::LegalRegistry<RegisteredForeign<UnitMeta>>(core, id, unitRegistryBasicRegistrySettings) {
}

void gen::UnitRegistry::addInstance(UnitMeta* meta, Unit* unit) {
	this->hasOrThrow(meta->identifier);
	if (meta->noInstances) {
		throw ArgumentException("Unit " + meta->identifier.literal + " does not support adding instances!");
	}
	this->instances[meta->identifier.hash]->push_back(unit);
}

void gen::UnitRegistry::registerUnit(gen_unit_loader::UnitMeta meta) {
	UnitMeta* item = new UnitMeta{ .type = meta.type, .identifier = identifier(meta.idLiteral), .name = meta.name, .noInstances = meta.noInstances };
	registerInternal(item->identifier, item);
}

void gen::UnitRegistry::registerInternal(gen::Identifier identifier, UnitMeta* meta) {
	this->add(identifier, RegisteredForeign<UnitMeta>::create(meta));
	if (!meta->noInstances) {
		this->instances[meta->identifier.hash] = new std::vector<Unit*>();
	} else {
		this->instances[meta->identifier.hash] = nullptr;
	}
}