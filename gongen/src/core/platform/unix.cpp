#include <gongen/core/platform.hpp>
#ifdef PLATFORM_UNIX
#include <dlfcn.h>
#include <string>

void* gen::loadLibrary(const char* name) {
	std::string name_str(name);
	void *out = dlopen(name, RTLD_LAZY);
	if (!out) {
		out = dlopen(std::string("lib" + name_str).c_str(), RTLD_LAZY);
		if (!out) {
			out = dlopen(std::string("lib" + name_str + ".so").c_str(), RTLD_LAZY);
			if (!out) {
				out = dlopen(std::string(name_str + ".so").c_str(), RTLD_LAZY);
				if (!out) {
					out = dlopen(std::string(name_str + ".so.1").c_str(), RTLD_LAZY);
				}
			}
		}
	}
	return out;
}

void gen::unloadLibrary(void* library) {
	if (library) dlclose(library);
}

void* gen::getProcAddress(void* library, const char* proc) {
	if (library) {
		return dlsym(library, proc);
	}
	return nullptr;
}

#include <gongen/core/file.hpp>
#include <gongen/core/exception.hpp>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

gen::FileHandle::FileHandle(const std::string& filepath, FileOpenMode openMode, bool throwExceptions) { open(filepath, openMode, throwExceptions); }
gen::FileHandle::~FileHandle() { close(); }

int platform_open(const char* cstr, int flags, int access) {
	return open(cstr, flags, access);
}

void platform_close(int descriptor) {
	close(descriptor);
}

void gen::FileHandle::open(const std::string& filepath, FileOpenMode openMode, bool throwExceptions) {
	if (isOpen()) close();
	int flags = 0;
	if (openMode == gen::FileOpenMode::APPEND) flags = O_CREAT | O_WRONLY | O_APPEND;
	else if (openMode == gen::FileOpenMode::READ) flags = O_RDONLY;
	else if (openMode == gen::FileOpenMode::READ_WRITE) flags = O_CREAT | O_RDWR;
	else if (openMode == gen::FileOpenMode::WRITE) flags = O_CREAT | O_WRONLY | O_TRUNC;

	mode = openMode;
	os_handle = (void*)platform_open(filepath.c_str(), flags, 0777);
	if (os_handle == (void*)(-1)) {
		os_handle = nullptr;
		if (throwExceptions) {
			throw gen::FileOpenException("Can't open file: " + filepath);
		}
	}
}
void gen::FileHandle::close() {
	if (isMapped()) {
		munmap(mapped_addr, size());
		mapping_handle = nullptr;
		mapped_addr = nullptr;
	}
	if (isOpen()) {
		platform_close(reinterpret_cast<intptr_t>(os_handle));
		os_handle = nullptr;
	}
}

bool gen::FileHandle::isOpen() { return (bool)os_handle; }
bool gen::FileHandle::isMapped() { return (bool)mapped_addr; }

uint64_t gen::FileHandle::size() {
	if (!isOpen()) return 0;
	uint64_t pos = cursor();
	uint64_t size = seek(0, FileSeekType::END);
	seek(pos);
	return size;
}
void gen::FileHandle::setSize(uint64_t size) {
	if (!isOpen()) return;
	ftruncate(reinterpret_cast<intptr_t>(os_handle), size);
}
uint64_t gen::FileHandle::seek(int64_t offset, FileSeekType type) {
	if (!isOpen()) return 0;
	int base = 0;
	if (type == FileSeekType::CUR) base = SEEK_CUR;
	else if (type == FileSeekType::SET) base = SEEK_SET;
	else if (type == FileSeekType::END) base = SEEK_END;
	return lseek(reinterpret_cast<intptr_t>(os_handle), offset, base);
}
uint64_t gen::FileHandle::cursor() {
	if (!isOpen()) return 0;
	return seek(0, gen::FileSeekType::CUR);
}

uint64_t platform_read(int fd, void *data, uint64_t dataSize) {
	return read(fd, data, dataSize);
}

uint64_t platform_write(int fd, void* data, uint64_t dataSize) {
	return write(fd, data, dataSize);
}

uint64_t gen::FileHandle::read(uint64_t size, void* data) {
	if (!isOpen()) return 0;
	if (!((uint32_t)mode & gen::internal::FLAG_READ)) return 0;
	return platform_read(reinterpret_cast<intptr_t>(os_handle), data, size);
}
uint64_t gen::FileHandle::write(uint64_t size, void* data) {
	if (!isOpen()) return 0;
	if (!((uint32_t)mode & gen::internal::FLAG_WRITE)) return 0;
	return platform_write(reinterpret_cast<intptr_t>(os_handle), data, size);
}
void gen::FileHandle::flush() {
	if (!isOpen()) return;
	fsync(reinterpret_cast<intptr_t>(os_handle));
}

void* gen::FileHandle::map(FileMapMode mode, uint64_t offset, uint64_t size, void* addr, bool throwExceptions) {
	if (!isOpen()) {
		if (throwExceptions) {
			throw gen::FileMapException("Can't map file: " + filepath);
		}
		return nullptr;
	}
	if (isMapped()) return mapped_addr;
	int prot = 0;
	uint64_t mapSize = 0;
	if (size == 0) mapSize = this->size();
	else mapSize = size;
	if (mode == FileMapMode::READ) prot = PROT_READ;
	else if (mode == FileMapMode::READ_WRITE) prot = PROT_READ | PROT_WRITE;
	mapping_handle = mmap(addr, mapSize, prot, MAP_PRIVATE, reinterpret_cast<intptr_t>(os_handle), offset);
	if (mapping_handle == (void*)(-1)) mapping_handle = nullptr;
	mapped_addr = mapping_handle;
	if (!mapped_addr) {
		if (throwExceptions) {
			throw gen::FileMapException("Can't map file: " + filepath);
		}
	}
	return mapped_addr;
}
void gen::FileHandle::mapFlush(uint64_t offset, uint64_t size) {
	if (isMapped()) {
		msync(mapped_addr, offset, MS_SYNC);
	}
}

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

uint64_t gen::FileHandle::creationTime() {
	if (!isOpen()) return 0;
	struct stat64 st;
	fstat64(reinterpret_cast<intptr_t>(os_handle), &st);
	return st.st_ctim.tv_sec;
}
uint64_t gen::FileHandle::lastModificationTime() {
	if (!isOpen()) return 0;
	struct stat64 st;
	fstat64(reinterpret_cast<intptr_t>(os_handle), &st);
	return st.st_mtim.tv_sec;
}
uint64_t gen::FileHandle::lastAccessTime() {
	if (!isOpen()) return 0;
	struct stat64 st;
	fstat64(reinterpret_cast<intptr_t>(os_handle), &st);
	return st.st_atim.tv_sec;
}

#include <sys/stat.h>

bool gen::pathExist(const char* path) {
	struct stat sb;
	return (stat(path, &sb) == 0);
}

bool gen::isDir(const char* path) {
	struct stat sb;
	if (stat(path, &sb) != 0) return false;
	uint32_t type = (sb.st_mode & S_IFMT);
	return (type == S_IFDIR);
}

#include <filesystem>
#include <algorithm>

bool gen::createDir(const char* path) {
	if (gen::isDir(path)) return false;
	std::string path_str = path;
	std::filesystem::path p = path_str;
	std::vector<std::string> pathsToCreate;
	pathsToCreate.push_back(path);
	while (true) {
		if (p.has_parent_path()) {
			p = p.parent_path();
			pathsToCreate.push_back(p.string());
		}
		else break;
	}

	std::reverse(pathsToCreate.begin(), pathsToCreate.end());
	for (uint32_t i = 0; i < pathsToCreate.size(); i++) {
		if (!gen::isDir(pathsToCreate[i].c_str())) {
			mkdir(pathsToCreate[i].c_str(), 0777);
		}
	}

	return true;
}

#include <dirent.h>

std::vector<gen::enumaratedFile> gen::enumarateDir(const char* dir) {
	std::vector<gen::enumaratedFile> out;
	DIR* dp;
	struct dirent* ep;
	std::string dir_str;
	if (dir) dir_str = dir;
	else dir_str = std::filesystem::current_path().string();

	dp = opendir(dir_str.c_str());
	if (dp != NULL) {
		ep = readdir(dp);
		while (ep) {
			gen::enumaratedFile f;
			f.name = std::string(ep->d_name);
			std::string p = dir_str + "/" + f.name;
			f.isDirectory = gen::isDir(p.c_str());
			if (dir) f.path = p;
			else f.path = f.name;
			out.push_back(f);
			ep = readdir(dp);
		}
		closedir(dp);
	}

	for (int32_t i = 0; i < out.size(); i++) {
		if ((!out[i].name.compare(".")) || (!out[i].name.compare(".."))) {
			out.erase(out.begin() + i);
			i--;
		}
	}

	return out;
}
#endif
