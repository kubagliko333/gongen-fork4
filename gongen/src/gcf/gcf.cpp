#include <gongen/gcf/gcf.hpp>
#include <string.h>

//Helper functions
uint8_t* gcfTempMemory(gen::GCFHandle& handle, uint64_t size) {
	if (size > handle.tempMemorySize) {
		if (handle.tempMemory) free(handle.tempMemory);
		handle.tempMemory = (uint8_t*)malloc(size);
		handle.tempMemorySize = size;
	}
	return handle.tempMemory;
}

#define GCF_ZSTD_SUPPORTED 1

#if GCF_ZSTD_SUPPORTED
	#include <zstd.h>
	
//Helper functions
uint64_t zstd_compress(void* src, void* dst, uint64_t srcSize, bool highCompressionLevel) {
	int compressionLevel = 1;
	if(highCompressionLevel) {
		compressionLevel = 6;
	}
	return ZSTD_compress(dst, ZSTD_compressBound(srcSize), src, srcSize, compressionLevel);
}

uint64_t zstd_compress(gen::GCFHandle &handle, void* src, uint64_t srcSize, bool highCompressionLevel) {
	return zstd_compress(src, gcfTempMemory(handle, ZSTD_compressBound(srcSize)), srcSize, highCompressionLevel);
}

void zstd_decompress(void* src, void* dst, uint64_t compressedSize, uint64_t uncompressedSize) {
	ZSTD_decompress(dst, uncompressedSize, src, compressedSize);
}

void zstd_decompress(gen::GCFHandle& handle, void* src, uint64_t compressedSize, uint64_t uncompressedSize) {
	return zstd_decompress(src, gcfTempMemory(handle, ZSTD_compressBound(uncompressedSize)), compressedSize, uncompressedSize);
}

#endif //GCF_LZ4_SUPPORTED

bool gen::openGCF(GCFHandle& handle, const std::string& filepath) {
	//Open
	/*if (vfs) {
		handle.useVFS = true;

		handle.vfsHandle = vfs->open(filepath.c_str(), gen::FileOpenMode::READ);
		if (!handle.vfsHandle->open) return false;
		if (vfs->fileSize(handle.vfsHandle) < sizeof(gen::GCFHeader1WithChecksum)) return false;
		handle.mappedFile = (uint8_t*)vfs->map(handle.vfsHandle, gen::MapMode::READ);
	}
	else {*/
		handle.useVFS = false;
		handle.fhandle.open(filepath.c_str(), gen::FileOpenMode::READ);
		if (!handle.fhandle.isOpen()) return false;
		if (handle.fhandle.size() < sizeof(gen::GCFHeader1WithChecksum)) return false;
		handle.mappedFile = (uint8_t*)handle.fhandle.map();
	//}
	if (!handle.mappedFile) return false;

	//Validate header
	uint32_t magic = ((uint32_t*)handle.mappedFile)[0];
	if (magic != GCF_MAGIC) return false;
	memcpy(&handle.header.header, handle.mappedFile, sizeof(GCFHeader1));
	if (handle.header.header.compatibleVersion > GCF_COMPATIBLE_VERSION) return false;
	uint32_t headerSize = handle.header.header.headerSize;

	//Check header checksum
	memcpy(&handle.header.checksum, &handle.mappedFile[headerSize], sizeof(Hash128));
	Hash128 headerHash = gen::hash128_stable(&handle.header.header, sizeof(GCFHeader1));
	if (memcmp(headerHash.data, handle.header.checksum.data, sizeof(uint64_t) * 2)) return false;

	//Setup GCFHandle
	handle.data = &handle.mappedFile[headerSize + (sizeof(uint64_t) * 2)];
	handle.loaded = true;

	//Load XML
	void* xmlData = nullptr;
	uint64_t xmlSize = 0;
	bool deleteXMLData = false;
	if (handle.header.header.xmlFlags & gen::GCFXMLFlag::GCF_XML_ZSTD_COMPRESSION) {
#if GCF_ZSTD_SUPPORTED
		zstd_decompress(handle, &handle.mappedFile[handle.header.header.xmlOffset], handle.header.header.xmlCompressedSize, handle.header.header.xmlUncompressedSize);
		xmlData = handle.tempMemory;
		xmlSize = handle.header.header.xmlUncompressedSize;
		deleteXMLData = false;
#else

#endif
	}
	else {
		xmlData = &handle.mappedFile[handle.header.header.xmlOffset];
		xmlSize = handle.header.header.xmlCompressedSize;
		deleteXMLData = false;
	}

	if (!xmlData) {

	}

	handle.xmlDoc.Clear();
	if (handle.xmlDoc.Parse((const char*)xmlData, xmlSize) != tinyxml2::XML_SUCCESS) {
		if (deleteXMLData) free(xmlData);
		return false;
	}

	if (deleteXMLData) free(xmlData);

	return true;
}

void gen::closeGCF(GCFHandle& handle) {
	handle.fhandle.close();
	handle.loaded = false;
}

void gen::freeGCF(GCFHandle& handle) {
	closeGCF(handle);
	if (handle.writtenData) {
		free(handle.writtenData);
		handle.writtenData = nullptr;
		handle.writtenDataPtr = 0;
		handle.writtenDataSize = 0;
	}
	if (handle.tempMemory) {
		free(handle.writtenData);
		handle.writtenData = nullptr;
		handle.tempMemorySize = 0;
	}
}

void gen::gcfGetData(GCFHandle& handle, void* data, uint64_t offset, uint64_t compressedSize, uint64_t uncompressedSize, gen::CompressionMode compressionMode) {
	if (compressionMode == gen::CompressionMode::UNCOMPRESSED) {
		memcpy(data, &handle.data[offset], compressedSize);
	}
	else if (compressionMode == gen::CompressionMode::ZSTD) {
#if GCF_ZSTD_SUPPORTED
		zstd_decompress(&handle.data[offset], data, compressedSize, uncompressedSize);
#else

#endif
	}
}

uint64_t gen::gcfAppendData(GCFHandle& handle, void* data, uint64_t dataSize, uint64_t *dataSizeOut, gen::CompressionMode compressionMode) {
	uint64_t dataSize2;
    void* data2;
    if (compressionMode == gen::CompressionMode::UNCOMPRESSED) {
        dataSize2 = dataSize;
        data2 = data;
    }
    else if (compressionMode == gen::CompressionMode::ZSTD) {
#if GCF_ZSTD_SUPPORTED
        dataSize2 = zstd_compress(handle, data, dataSize, false);
        data2 = handle.tempMemory;
#else
#endif
    }
    if ((handle.writtenDataPtr + dataSize2) > handle.writtenDataSize) {
        handle.writtenDataSize = (handle.writtenDataPtr + dataSize2);
        handle.writtenData = (uint8_t*)realloc(handle.writtenData, handle.writtenDataSize);
        memcpy(&handle.writtenData[handle.writtenDataPtr], data2, dataSize2);
        handle.writtenDataPtr = handle.writtenDataPtr + dataSize2;
    }
    if (dataSizeOut) (*dataSizeOut) = dataSize2;
    return (handle.writtenDataPtr - dataSize2);
}

void gen::saveGCF(GCFHandle& handle, const std::string& filepath) {
	closeGCF(handle);
	handle.fhandle.open(filepath.c_str(), gen::FileOpenMode::WRITE);

	//Setup XML
	tinyxml2::XMLPrinter printer(nullptr, false);
	handle.xmlDoc.Print(&printer);

	//Setup Header
	handle.header.header.xmlOffset = handle.writtenDataPtr + sizeof(gen::GCFHeader1WithChecksum);
	handle.header.header.xmlCompressedSize = (printer.CStrSize() - 1);
	handle.header.header.xmlUncompressedSize = handle.header.header.xmlCompressedSize;

	void* xmlData = (void*)printer.CStr();
	if (handle.header.header.xmlFlags & gen::GCFXMLFlag::GCF_XML_ZSTD_COMPRESSION) {
#if GCF_ZSTD_SUPPORTED
		handle.header.header.xmlCompressedSize = zstd_compress(handle, (void*)printer.CStr(), handle.header.header.xmlUncompressedSize, false);
		xmlData = handle.tempMemory;
#else

#endif
	}

	handle.header.checksum = gen::hash128_stable(&handle.header.header, sizeof(gen::GCFHeader1));

	//Write
	handle.fhandle.write(sizeof(gen::GCFHeader1WithChecksum), &handle.header);
	handle.fhandle.write(handle.writtenDataPtr, handle.writtenData);
	handle.fhandle.write(handle.header.header.xmlCompressedSize, xmlData);
	handle.fhandle.flush();
}
