#pragma once

#include <string>
#include <unordered_map>
#include <limits.h>
#include <vector>
#include <memory>
#include <gongen/core/file.hpp>
#include <gongen/core/export.hpp>
#include <tinyxml2.h>

namespace gen {
	/*struct GCFHandle;

	enum class VFSNodeType {
		PHYSICAL, //on the disc
		GCF //inside GCF 
	};

	const static uint32_t INVALID_VFS_NODE = UINT32_MAX;

	struct GEN_EXPORTED VFSNode {
		VFSNodeType type;

		std::string physicalPath;
		std::string name;
		bool directory;

		uint64_t gcfOffset;
		uint64_t gcfCompressedSize;
		uint64_t gcfUncompressedSize;
		uint32_t gcfCompression;
		uint64_t gcfID;
	};
	
	struct GEN_EXPORTED VFSHandle {
		VFSNodeType type;

		bool open;

		gen::FileHandle fhandle;

		uint64_t gcfOffset;
		uint64_t gcfCompressedSize;
		uint64_t gcfUncompressedSize;
		uint32_t gcfCompression;
		gen::GCFHandle* gcf;

		VFSHandle() {}
		~VFSHandle() {
			if (type == gen::VFSNodeType::PHYSICAL) {
				if (gen::isFileOpen(fhandle)) {
					gen::closeFile(fhandle);
				}
				if (fhandle.mapped_addr) {
					gen::unmapFile(fhandle, fhandle.mapped_addr);
				}
			}
		}
	};

	typedef std::shared_ptr<VFSHandle> VFSHandlePtr;*/

	class GEN_EXPORTED VFS {
	public:
		// Not Thread-Safe (Don't call if any other thread is calling any VFS function)
		/*virtual void mount(const std::string& realPath, const std::string& vfsPath) = 0;
		virtual void mount(gen::GCFHandle* handle, const std::string& vfsPath) = 0;
		virtual void unmountAll() = 0;*/

		// Thread-Safe (warning compressed files can't be mapped and read or write partially)
		/*virtual VFSHandlePtr open(const std::string& path, gen::FileOpenMode mode) = 0;
		virtual void* map(VFSHandlePtr& handle, gen::MapMode mode) = 0;
		virtual uint64_t fileSize(VFSHandlePtr& handle) = 0;
		virtual uint64_t read(VFSHandlePtr& handle, uint64_t offset, uint64_t size, void* data) = 0;
		virtual uint64_t write(VFSHandlePtr& handle, uint64_t offset, uint64_t size, void* data) = 0;

		virtual bool isDir(const std::string& path) = 0;
		virtual std::vector<gen::enumaratedFile> enumarate(const std::string& path) = 0;

		virtual void* map(VFSHandlePtr& handle) = 0;

		virtual bool isCompressed(VFSHandlePtr& handle) = 0;
		virtual bool isMappable(VFSHandlePtr& handle) = 0; */
	};

	GEN_EXPORTED VFS* createVFS();
}
