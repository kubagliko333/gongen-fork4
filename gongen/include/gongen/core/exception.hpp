#pragma once

#include "export.hpp"
#include <string>

namespace gen {

#define GEN_EXCEPTION(CLASS_NAME, PARENT, NAME) \
class GEN_EXPORTED CLASS_NAME : public PARENT { \
protected: \
	CLASS_NAME(std::string name, std::string message, const Exception* cause) : PARENT(name, message, cause) { \
	} \
public: \
	CLASS_NAME(std::string message, const Exception* cause) : CLASS_NAME(NAME, message, cause) { \
	} \
	CLASS_NAME(std::string message) : CLASS_NAME(message, nullptr) { \
	} \
};


class GEN_EXPORTED Exception {
public:
	const std::string name;
	const std::string message;
	const Exception* cause;

	Exception(std::string name, std::string message, const Exception* cause) : name(name), message(message), cause(cause) {
	}
	Exception(std::string mesasge, const Exception* cause) : Exception("GongEn Exception", message, cause) {
	}
	Exception(std::string mesasge) : Exception(message, nullptr) {
	}
};

class GEN_EXPORTED StdBasedException final : public Exception {
public:
	const std::exception* original;

	StdBasedException(const std::exception* original) : original(original), Exception("C++ Standard Exception", original->what(), nullptr) {
	}
};

GEN_EXCEPTION(IOException, Exception, "IO Exception")
GEN_EXCEPTION(FileException, IOException, "File Exception")
GEN_EXCEPTION(FileOpenException, FileException, "File Open Exception")
GEN_EXCEPTION(FileMapException, FileException, "File Map Exception")

GEN_EXCEPTION(FunctionCallException, Exception, "Bad Function Call Error")
GEN_EXCEPTION(ArgumentException, FunctionCallException, "Invalid or Illegal Argument Error")
GEN_EXCEPTION(FunctionAccessException, FunctionCallException, "Illegal Function Access Error")
GEN_EXCEPTION(ObjectStateException, FunctionCallException, "Bad Object State Error") // method called with bad object state (e.g object is not prepared)

GEN_EXCEPTION(HardwareException, Exception, "Missing Hardware or Hardware function")
GEN_EXCEPTION(NoSuitableDeviceException, HardwareException, "No Suitable Device")
GEN_EXCEPTION(ExtensionSupportException, NoSuitableDeviceException, "Missing Required Device Extension")
GEN_EXCEPTION(FeatureSupportException, NoSuitableDeviceException, "Missing Required Device Feature")

GEN_EXCEPTION(GraphicsRuntimeException, FunctionCallException, "Graphics Implementation Runtime Exception")
GEN_EXCEPTION(GraphicsResourceCreationException, GraphicsRuntimeException, "Graphics Resource Creation Exception")
GEN_EXCEPTION(GraphicsCommandException, GraphicsRuntimeException, "Invalid Graphics Command")
GEN_EXCEPTION(GraphicsCommandExecutionException, GraphicsRuntimeException, "Exception During Graphics Command Execution")

#undef GEN_EXCEPTION

}
