#include <gongen/core/core.hpp>
#include <gongen/core/random.hpp>
#include <gongen/window_api/window.hpp>
#include <gongen/gcf/gcf.hpp>
#include <stdio.h>
#include <string>
#include <stb_vorbis.c>
#include <gongen/core/file.hpp>
#include <gongen/gcf/gcf.hpp>
#include <gongen/graphics_api/graphics.hpp>
#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_PNG
#include <stb_image.h>
#include <gongen/imgui/imgui.hpp>
#include <gongen/core/exception.hpp>
#include <gongen/vfs/vfs.hpp>

extern gen::WindowAPI* glfwCreateWindowAPI();
extern gen::GraphicsAPI* vulkanCreateGraphicsAPI();

void printEvent(gen::Event& e);

gen::ShaderDesc loadShaderFromGCF(gen::GCFHandle &handle, const std::string shaderName, std::vector<std::pair<std::string, std::string>> macros, gen::ShaderType type);

#include <math.h>

#define degToRad(angleInDegrees) ((angleInDegrees) * M_PI / 180.0)

int example_main();

int main() {
	try {
		return example_main();
	}
	catch (gen::Exception& exception) {
		printf("%s: %s\n", exception.name.c_str(), exception.message.c_str());
	}
}

int example_main() {
	// GongEn instance creation
	gen::CoreBuilder builder;
	builder.addLoggerCallback(gen::identifier("gen::standard_out"));
	gen::Core* gongen = builder.build();

	// Logger creation and example usage
	const gen::Logger* logger = gongen->addLogger((new gen::LoggerBuilder(gongen, gen::identifier("example::logger")))->setName("Example logger"));
	logger->info("Testing logger o7");

	// Hashes example
	std::string testStr = "Hello World from Bitrium";
	std::string testStr2 = "This String is False";

	uint32_t hash1 = gen::hash32((void*)testStr.c_str(), testStr.size());
	uint32_t hash2 = gen::hash32((void*)testStr2.c_str(), testStr2.size());
	uint64_t hash3 = gen::hash64((void*)testStr.c_str(), testStr.size());
	uint64_t hash4 = gen::hash64((void*)testStr2.c_str(), testStr2.size());

	printf("%u\n", hash1);
	printf("%u\n", hash2);
	printf("%llu\n", hash3);
	printf("%llu\n", hash4);

	gen::Hash128 hash5;
	gen::hash128_stable(hash5.data, (void*)testStr.c_str(), testStr.size());

	printf("%llu : %llu \n", hash5.data[0], hash5.data[1]);

	// Identifiers example
	gen::Identifier identifier = gen::identifier("example::test");
	printf("%llu\n", identifier.hash);
	printf("%s\n", identifier.namespaceId.c_str());
	printf("%s\n", identifier.itemId.c_str());

	gen::BlanketIdentifier blanketIdentifier = gen::blanketIdentifier("thing::example::test");
	printf("%llu\n", blanketIdentifier.hash);
	printf("%s\n", blanketIdentifier.registryId.c_str());
	printf("%s\n", blanketIdentifier.namespaceId.c_str());
	printf("%s\n", blanketIdentifier.itemId.c_str());

	// Registries example
	gen::Registry<gen::RegisteredForeign<std::string>>* registry = gongen->addRegistry((new gen::LegalRegistryBuilder<gen::RegisteredForeign<std::string>>("thing"))->restrictRemoving());
	//registry->add(identifier, new std::string("Example value"));
	//registry->add(gen::identifier("example::another"), new std::string("Another value"));

	// Random example
	gen::Random random(hash3);
	for (int i = 0; i < 4; i++) {
		printf("%u\n", random.nextUint32(i*100, (i+1)*100));
	}

	//FileAPI Test
	std::string executableDir = gen::getExecutableDir();
	std::string prefDir = gen::getPrefDir("Bitrium", "ExampleApp1");
	std::vector<gen::enumaratedFile> enumaratedFiles = gen::enumarateDir("data");

	gen::FileHandle testFile("shaders.gcf", gen::FileOpenMode::READ);
	uint64_t creationTime = testFile.creationTime();
	uint64_t modificationTime = testFile.lastModificationTime();
	uint64_t accessTime = testFile.lastAccessTime();

	//VFS Test
	/*gen::VFS* vfs = gen::createVFS();
	vfs->mount("data", "");
	vfs->mount("test.gcf", "");
	vfs->mount("", "");
	std::vector<gen::enumaratedFile> enumaratedFiles2 = vfs->enumarate("CMakeFiles");*/

	//GCF Test
	gen::GCFHandle handle;
	tinyxml2::XMLElement* pRoot = handle.xmlDoc.NewElement("GCF");
	handle.xmlDoc.InsertEndChild(pRoot);
	tinyxml2::XMLElement* pNode = pRoot->InsertNewChildElement("Test");
	uint32_t data = 42;
	gen::gcfAppendData(handle, (uint8_t*)&data, sizeof(uint32_t), nullptr, gen::CompressionMode::ZSTD);
	handle.header.header.xmlFlags = gen::GCFXMLFlag::GCF_XML_ZSTD_COMPRESSION;
	gen::saveGCF(handle, "test.gcf");
	gen::closeGCF(handle);

	gen::openGCF(handle, "test.gcf");
	gen::freeGCF(handle);

	// GLFW Windows example
	gen::WindowAPI* windowAPI = glfwCreateWindowAPI();
	gen::Window* window = windowAPI->createWindow({ gen::WindowType::WINDOWED, 1280, 720 });

	//Graphics Test
	gen::GraphicsAPI *graphicsAPI = vulkanCreateGraphicsAPI();
	graphicsAPI->init(windowAPI, { true, false });

	gen::Swapchain* swapchain;
	gen::GraphicsDevice* graphicsDevice = graphicsAPI->createDevice(nullptr, window, &swapchain);

	gen::BufferPtr buffer1 = graphicsDevice->createBuffer({ gen::BufferBindFlag::BUFFER_BIND_VERTEX_BUFFER, sizeof(float) * 6, gen::BufferType::STATIC });

	gen::CommandBufferPtr commandBuffer = graphicsDevice->getCommandBuffer();

	float vertexData[] = {
		-0.5f, -0.5f,
		0.5f, -0.5f,
		0.0f,  0.5f,
	};

	commandBuffer->updateBuffer(buffer1, 0, sizeof(float) * 6, vertexData);

	gen::BufferPtr indirectBuffer = graphicsDevice->createBuffer({ gen::BufferBindFlag::BUFFER_BIND_INDIRECT_BUFFER, sizeof(gen::drawIndirectCommand), gen::BufferType::STATIC });

	gen::drawIndirectCommand indirectCmd = {3, 1, 0, 0};

	commandBuffer->updateBuffer(indirectBuffer, 0, sizeof(gen::drawIndirectCommand), &indirectCmd);

	gen::TextureDesc texture0_desc;
	int w = 0, h = 0, comp = 0;
	uint8_t *imageData = stbi_load("data/test.png", &w, &h, &comp, 4);
	texture0_desc.width = w;
	texture0_desc.height = h;
	texture0_desc.bindFlags = gen::TextureBindFlag::TEXTURE_BIND_TRANSFER_DST | gen::TextureBindFlag::TEXTURE_BIND_SAMPLED;
	gen::TexturePtr texture0 = graphicsDevice->createTexture(texture0_desc);
	commandBuffer->updateTexture(texture0, gen::TextureOffset(), w, h, 1, imageData);

	gen::GCFHandle shadersHandle;
	if (!gen::openGCF(shadersHandle, "shaders.gcf")) {
		throw gen::Exception("Can't open shaders.gcf");
	}
	gen::ShaderPtr shader0 = graphicsDevice->createShader(loadShaderFromGCF(shadersHandle, "test_shader", { { "USE_TEXTURE", "1" }, {"USE_UBO", "1"} }, graphicsAPI->getShaderType()));

	gen::PipelineDesc pipeline0_desc;
	pipeline0_desc.viewport.width = window->getWidth();
	pipeline0_desc.viewport.height = window->getHeight();
	pipeline0_desc.scissor.width = window->getWidth();
	pipeline0_desc.scissor.height = window->getHeight();
	pipeline0_desc.vertexInput.bindings[0].stride = (sizeof(float) * 2);
	pipeline0_desc.vertexInput.bindings[0].perInstance = false;
	pipeline0_desc.vertexInput.attributes[0].binding = 0;
	pipeline0_desc.vertexInput.attributes[0].format = gen::GPUFormat::RG32_SFLOAT;
	pipeline0_desc.vertexInput.attributes[0].offset = 0;
	pipeline0_desc.blend.attachments[0].enabled = true;
	pipeline0_desc.shader = shader0;

	float angle = 0.0f;

	gen::ShaderPtr imguiShader = graphicsDevice->createShader(loadShaderFromGCF(shadersHandle, "imgui", {}, graphicsAPI->getShaderType()));
	gen::ImGui_Init(window, graphicsDevice, imguiShader);

	while (window->isOpen()) {
		if (!swapchain->isMinimized()) windowAPI->poolEvents();
		else windowAPI->waitEvents();
		for (auto& e : windowAPI->events) {
			printEvent(e);
			gen::ImGui_ProcessEvent(e);

			if (e.type == gen::EventType::WINDOW_SIZE) {
				swapchain->handleResize(e.window.resize.w, e.window.resize.h);
			}
		}

		gen::ImGui_NewFrame(commandBuffer);

		ImGui::ShowDemoWindow();
		ImGui::ShowAboutWindow();
		ImGui::ShowStyleEditor();
		ImGui::ShowMetricsWindow();
		ImGui::ShowStackToolWindow();
		ImGui::ShowUserGuide();

		float radAngle = degToRad(angle);

		float matrix[16] = {
			cosf(radAngle), -sin(radAngle), 0, 0,
			sin(radAngle), cos(radAngle), 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
		};

		radAngle = degToRad(angle + 90.0f);

		float matrix2[16] = {
			cosf(radAngle), -sin(radAngle), 0, 0,
			sin(radAngle), cos(radAngle), 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
		};

		radAngle = degToRad(angle + 180.0f);

		float matrix3[16] = {
			cosf(radAngle), -sin(radAngle), 0, 0,
			sin(radAngle), cos(radAngle), 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
		};

		radAngle = degToRad(angle + 270.0f);

		float matrix4[16] = {
			cosf(radAngle), -sin(radAngle), 0, 0,
			sin(radAngle), cos(radAngle), 0, 0,
			0, 0, 1, 0,
			0, 0, 0, 1
		};

		if (!swapchain->isMinimized()) {
			gen::BufferPtr uniformBuffer = commandBuffer->createDynamicBuffer(gen::BufferBindFlag::BUFFER_BIND_UNIFORM_BUFFER, sizeof(float) * 16);
			commandBuffer->updateBuffer(uniformBuffer, 0, sizeof(float) * 16, matrix);

		gen::BufferPtr uniformBuffer2 = commandBuffer->createDynamicBuffer(gen::BufferBindFlag::BUFFER_BIND_UNIFORM_BUFFER, sizeof(float) * 16);
		commandBuffer->updateBuffer(uniformBuffer2, 0, sizeof(float) * 16, matrix2);

		gen::BufferPtr uniformBuffer3 = commandBuffer->createDynamicBuffer(gen::BufferBindFlag::BUFFER_BIND_UNIFORM_BUFFER, sizeof(float) * 16);
		commandBuffer->updateBuffer(uniformBuffer3, 0, sizeof(float) * 16, matrix3);

		gen::BufferPtr uniformBuffer4 = commandBuffer->createDynamicBuffer(gen::BufferBindFlag::BUFFER_BIND_UNIFORM_BUFFER, sizeof(float) * 16);
		commandBuffer->updateBuffer(uniformBuffer4, 0, sizeof(float) * 16, matrix4);

		angle += 1.0f;

		commandBuffer->beginRenderPass(swapchain->getRenderPass(commandBuffer.get()));
			commandBuffer->bindPipeline(pipeline0_desc);
			commandBuffer->bindVertexBuffers(buffer1, 0, 0);
			commandBuffer->setUniformBuffer(0, 0, uniformBuffer, 0, 0);
			commandBuffer->setTexture(0, 1, texture0);
			commandBuffer->setSampler(0, 2, { gen::TextureFilter::NEAREST, gen::TextureFilter::NEAREST });
			commandBuffer->drawIndirect(indirectBuffer, 1);

			commandBuffer->setUniformBuffer(0, 0, uniformBuffer2, 0, 0);
			commandBuffer->drawIndirect(indirectBuffer, 1);

			commandBuffer->setUniformBuffer(0, 0, uniformBuffer3, 0, 0);
			commandBuffer->drawIndirect(indirectBuffer, 1);

			commandBuffer->setUniformBuffer(0, 0, uniformBuffer4, 0, 0);
			commandBuffer->drawIndirect(indirectBuffer, 1);

			gen::ImGui_Render(commandBuffer);
		commandBuffer->endRenderPass();
		commandBuffer->execute(0);

			commandBuffer->display(swapchain);

			commandBuffer = graphicsDevice->getCommandBuffer();
		}
	}

	return 0;
}

void loadShaderFromGCF_variant(gen::ShaderDesc& desc, gen::GCFHandle& handle, tinyxml2::XMLElement* pNode, gen::ShaderType type) {
	tinyxml2::XMLElement* pSubNode = pNode->FirstChildElement();
	while (pSubNode) {
		bool validStage = false;
		gen::ShaderStage stage;
		if (!strcmp(pSubNode->Name(), "VertexShader")) {
			stage = gen::ShaderStage::VERTEX_SHADER;
			validStage = true;
		}
		else if (!strcmp(pSubNode->Name(), "HullShader")) {
			stage = gen::ShaderStage::HULL_SHADER;
			validStage = true;
		}
		else if (!strcmp(pSubNode->Name(), "DomainShader")) {
			stage = gen::ShaderStage::DOMAIN_SHADER;
			validStage = true;
		}
		else if (!strcmp(pSubNode->Name(), "GeometryShader")) {
			stage = gen::ShaderStage::GEOMETRY_SHADER;
			validStage = true;
		}
		else if (!strcmp(pSubNode->Name(), "PixelShader")) {
			stage = gen::ShaderStage::PIXEL_SHADER;
			validStage = true;
		}
		else if (!strcmp(pSubNode->Name(), "ComputeShader")) {
			stage = gen::ShaderStage::COMPUTE_SHADER;
			validStage = true;
		}

		if (validStage) {
			gen::ShaderStage_t* stagePtr = &desc.stages[(size_t)stage];

			tinyxml2::XMLElement* pSubNode3 = pSubNode->FirstChildElement();
			while (pSubNode3) {
				if (!strcmp(pSubNode3->Name(), "Descriptor")) {
					uint32_t set = pSubNode3->UnsignedAttribute("set");
					uint32_t slot = pSubNode3->UnsignedAttribute("slot");
					uint32_t type = pSubNode3->UnsignedAttribute("type");

					stagePtr->metadata.descriptors[set][slot] = (gen::SPIRVDescriptorType)type;
				}
				pSubNode3 = pSubNode3->NextSiblingElement();
			}

			tinyxml2::XMLElement* pSubNode2 = pSubNode->FirstChildElement();
			while (pSubNode2) {
				if (!strcmp(pSubNode2->Name(), "SPIRV")) {
					if (type == gen::ShaderType::SPIRV) {
						uint32_t dataSize = pSubNode2->UnsignedAttribute("dataSize");
						uint32_t uncompressedSize = pSubNode2->UnsignedAttribute("uncompressedSize");
						uint32_t dataOffset = pSubNode2->UnsignedAttribute("dataOffset");
						stagePtr->spirv.resize(uncompressedSize / sizeof(uint32_t));
						gen::gcfGetData(handle, stagePtr->spirv.data(), dataOffset, dataSize, uncompressedSize);
					}
				}
				pSubNode2 = pSubNode2->NextSiblingElement();
			}
		}

		pSubNode = pSubNode->NextSiblingElement();
	}
}

gen::ShaderDesc loadShaderFromGCF_func(gen::GCFHandle& handle, tinyxml2::XMLElement* pNode, const std::string shaderName, std::vector<std::pair<std::string, std::string>> macros, gen::ShaderType type) {
	gen::ShaderDesc out;
	out.stageFlags = 0;
	tinyxml2::XMLElement* pSubNode = pNode->FirstChildElement();
	while (pSubNode) {
		if (!strcmp(pSubNode->Name(), "Node")) {
			if (!strcmp(pSubNode->Attribute("type"), "shader")) {
				if (!strcmp(pSubNode->Attribute("name"), shaderName.c_str())) {
					uint32_t stages = pSubNode->UnsignedAttribute("stages");
					out.stageFlags = stages;
					tinyxml2::XMLElement* pSubNode2 = pSubNode->FirstChildElement();
					while (pSubNode2) {
						if (!strcmp(pSubNode2->Name(), "Variant")) {
							tinyxml2::XMLElement* pSubNode3 = pSubNode2->FirstChildElement();

							std::vector<std::pair<std::string, std::string>> macros2;

							while (pSubNode3) {
								if (!strcmp(pSubNode3->Name(), "Macro")) {
									macros2.push_back(std::make_pair(pSubNode3->Attribute("name"), pSubNode3->Attribute("value")));
								}
								pSubNode3 = pSubNode3->NextSiblingElement();
							}

							bool variantSame = true;
							if (macros.size() != macros2.size()) {
								variantSame = false;
							}
							else {
								for (uint32_t i = 0; i < macros.size(); i++) {
									bool found = false;
									for (auto& m : macros2) {
										if (macros[i] == m) found = true;
									}
									if (!found) variantSame = false;
								}
							}
							if (variantSame) {
								loadShaderFromGCF_variant(out, handle, pSubNode2, type);
							}
						}
						pSubNode2 = pSubNode2->NextSiblingElement();
					}
				}
			}
		}
		pSubNode = pSubNode->NextSiblingElement();
	}
	return out;
}

gen::ShaderDesc loadShaderFromGCF(gen::GCFHandle &handle, const std::string shaderName, std::vector<std::pair<std::string, std::string>> macros, gen::ShaderType type) {
	tinyxml2::XMLElement* pRoot = handle.xmlDoc.FirstChildElement("GCF");

	return loadShaderFromGCF_func(handle, pRoot, shaderName, macros, type);
}

void printEvent(gen::Event& e) {
	if (e.type == gen::EventType::KEY) {
		printf("Key: %u, Mod: %u, Action: %u\n", e.key.key, e.key.mods, e.key.action);
	}
	else if (e.type == gen::EventType::CHARACTER) {
		printf("Char: %u\n", e.character.codepoint);
	}
	if (e.type == gen::EventType::MOUSE_POSITION) {
		printf("Mouse: %f,%f\n", e.mousePosition.x, e.mousePosition.y);
	}
	if (e.type == gen::EventType::MOUSE_BUTTON) {
		printf("MouseButton: %u, Action: %u\n", e.mouseButton.button, e.mouseButton.action);
	}
	if (e.type == gen::EventType::MOUSE_WHEEL) {
		printf("MouseWheel: %f,%f\n", e.mouseWheel.x, e.mouseWheel.y);
	}
	if (e.type == gen::EventType::FILE_DROP) {
		for (auto& p : e.fileDrop.paths) {
			printf("DropPath: %s\n", p.c_str());
		}
		gen::FileHandle file(e.fileDrop.paths[0].c_str(), gen::FileOpenMode::READ);
		std::string str;
		str.resize(file.size());
		file.read(str.size(), (void*)str.data());
		printf("FileContent: %s\n", str.c_str());
	}
}
